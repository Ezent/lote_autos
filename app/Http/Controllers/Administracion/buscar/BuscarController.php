<?php

namespace App\Http\Controllers\Administracion\buscar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BuscarController extends Controller
{
    /*Devolver lista*/
    
    public function datos(Request $request){
        if($request->ajax()){
            $data=DB::table('buscar')->where('nombre','like',$request->buscar.'%')->orWhere('nombre','like','%'.$request->buscar.'%')->
                    orderBy('nombre','asc')->get();
//            dd($data);
            return response()->json($data);
        }
    }
    
}
