<?php

namespace App\Http\Controllers\Administracion\panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    
    public function index(){
        return view('administracion/login/administrador');
    }
    public function panel_login(Request $request){
        if (Auth::attempt(['correo'=>$request->correo,'password'=>$request->pass])) {
            return redirect()->route('panel');
        } else {
            return back()->withErrors(['error' => 'Estas credenciales no son correctas!!!'])
                    ->withInput(request(['correo']));
        }
    }
    public function panel_logout() {
        Auth::logout();
        return redirect()->route('startLogin');
    }
}
