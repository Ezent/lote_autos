<?php
namespace App\Http\Controllers\Administracion\panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PublicacionesController extends Controller
{
    public function __construct(){
    $this->middleware('auth',['only'=>'slider']);
    }
    public function slider(Request $request){
        $sliders=DB::table('slider')->get();
        return view('administracion.panel.panel_slider',['slider'=>$sliders]);
    }

    function new(Request $request){
        if($request->file('img')){
            $archivo=$request->file('img');
            $name=rand().'_'.$request->img->getClientOriginalName();
            $archivo->move(public_path().'/images/BD_imagenes/',$name);
            DB::table('slider')->insert([
                'imagen'=>'images/BD_imagenes/'.$name,
                'descripcion'=>($request->descripcion==' ' || $request->descripcion==null)? null: $request->descripcion
            ]);
            return redirect()->route('slider')->with(['msj'=>'Insertado Correctamente']);
        }
    }
        
        function edit(Request $request){
            if($request->file('img')){
                $archivo=$request->file('img');
                $name=rand().'_'.$request->img->getClientOriginalName();
                unlink($request->anterior_img);//Eliminar imagen que se va a sustituir
                $archivo->move(public_path().'/images/BD_imagenes/',$name);
                $datos['imagen']='images/BD_imagenes/'.$name;
            }
            if(!$request->descripcion=='' || !$request->descripcion==null){
                $datos['descripcion']=$request->descripcion;
            }
            DB::table('slider')->where('id','=',$request->id)->update($datos);
            
            return redirect()->route('slider')->with(['msj'=>'Editado correctamente']);
        }
        function delete(Request $request){
            unlink($request->img_anterior_delete);
            DB::table('slider')->where('id','=',$request->id)->delete();
            return redirect()->route('slider')->with(['msj'=>'Eliminado Correctamente']);
        }

        /*ver publicaciones pagina de Inicio */
        function starPage(Request $request){
            $post=DB::table('slider')->get();
            return view('plantilla/start')->with('post',$post);
        }
}
    