<?php

namespace App\Http\Controllers\Administracion\panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PanelController extends Controller {
    /*Constructor*/
    public function __construct(){
        $this->middleware('auth');
    }

    public function index() {
        /* Consultas */
        $datos = DB::table('inventario_vehiculos as iv')->join('vehiculos as v', 'iv.id', '=', 'v.inv_id')->
                        select('iv.id as id_cat', 'iv.categoria', 'v.id as id_vehiculos', 'v.img', 'v.nombre','v.precio', 'v.descripcion', 'v.registro')->get();
//        dd($datos);
        return view('administracion.panel.panel', ['datos' => $datos]);
    }

    public function vehiculos() {
        /* agregado informacion requerida para la vista */
        $categorias = DB::table('inventario_vehiculos')->get();
        return view('administracion.panel.panel_vehiculos', ['categoria' => $categorias]);
    }

    /* Insertar Vehiculos */

    public function addVehiculos(Request $request) {
        /* Validadr las imagenes */
        $name = "";
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $name = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/images/BD_imagenes/', $name);
        }
        DB::table('vehiculos')->insert(
                [
                    'img' => 'images/BD_imagenes/' . $name,
                    'inv_id' => $request->categoria,
                    'nombre' => $request->nombre,
                    'precio' => $request->precio,
                    'descripcion' => ($request->descripcion == "" || $request->descripcion == null) ? 'NO' : $request->descripcion
        ]);
        //obtener primero el id del vehiculo insertado
        $id_vehiculo = DB::select('call vehiculos_id()');
        /* Regla de  validacion para checar en cual de los dos tipos se va insertar el archivo */
        if (is_array($request->interior_img)) {
            $indice = array_keys($request->interior_img);
            for ($j = 0; $j < count($indice); $j++) {
                if (file($request->interior_img[$indice[$j]])) {
                    $file_int = $request->interior_img[$indice[$j]];
                    $name_int = 'int_' . $id_vehiculo[0]->id . '_' . rand() . $file_int->getClientOriginalName();
                    $file_int->move(public_path() . '/images/BD_imagenes/', $name_int);
                    DB::table('interiores')->insert([
                    'vehiculos_id' => $id_vehiculo[0]->id,
                    'interiores' => 'images/BD_imagenes/' . $name_int,
                    'descripcion' => ($request->interior_desc[$indice[$j]] == "" || $request->interior_desc[$indice[$j]] == null) ? 'NO' : $request->interior_desc[$indice[$j]],
                ]);
                }
            }
        }
        /* Validar que los exteriores lleven informacion */
      if (is_array($request->exterior_img)) {
            $indice_ext = array_keys($request->exterior_img);
            for ($j = 0; $j < count($indice_ext); $j++) {
                if (file($request->exterior_img[$indice_ext[$j]])) {
                    $file_ext = $request->exterior_img[$indice_ext[$j]];
                    $name_ext = 'ext_' . $id_vehiculo[0]->id . '_' . rand() . $file_ext->getClientOriginalName();
                    $file_ext->move(public_path() . '/images/BD_imagenes/', $name_ext);
                    DB::table('exteriores')->insert([
                    'vehiculos_id' => $id_vehiculo[0]->id,
                    'exteriores' => 'images/BD_imagenes/' . $name_ext,
                    'descripcion' => ($request->exterior_desc[$indice_ext[$j]] == "" || $request->exterior_desc[$indice_ext[$j]] == null) ? 'NO' : $request->exterior_desc[$indice_ext[$j]],
                ]);
                }
            }
        }
        
        $msj = 'Insertado correctamente';
//    return redirect()->route('panel_vehiculos',['msj'=>$msj]);
        return redirect()->route('panel_vehiculos')->with(compact('msj'));
    }

    public function viewVehiculos(Request $request) {
        $vehiculo = DB::table('vehiculos')->where('id', '=', $request->id)->first();
        $int = DB::table('vehiculos as v')->join('interiores as i', 'v.id', '=', 'i.vehiculos_id')->where('v.id', '=', $request->id)->
                        select('v.img', 'v.nombre', 'v.descripcion', 'i.interiores', 'i.descripcion as descInt')->get();
        $ext = DB::table('vehiculos as v')->join('exteriores as e', 'v.id', '=', 'e.vehiculos_id')->where('v.id', '=', $request->id)->
                        select('v.img', 'v.nombre', 'v.descripcion', 'e.exteriores', 'e.descripcion as descExt')->get();
        $arreglo = array('interiores' => $int, 'exteriores' => $ext, 'vehiculo' => $vehiculo); /* Unir los tres arreglos */
        return response()->json($arreglo);
    }

    public function dropVehiculos(Request $request) {
        /* Eliminar archivos */
        $vehiculo=DB::table('vehiculos')->where('id','=',$request->vehiculos_id)->select('img')->first();
        $int=DB::table('interiores')->where('vehiculos_id','=',$request->vehiculos_id)->select('interiores')->get();
        $ext=DB::table('exteriores')->where('vehiculos_id','=',$request->vehiculos_id)->select('exteriores')->get();
        if($vehiculo){
            unlink($vehiculo->img);
        }
        if(!$int->isEmpty()){
            for ($i=0; $i < count($int); $i++) { 
            unlink($int[$i]->interiores);
            }
        }
        if(!$ext->isEmpty()){
            for ($i=0; $i < count($ext); $i++) { 
                unlink($ext[$i]->exteriores);
            }
        }
      /*   $listDrop = DB::table('vehiculos as v')->leftJoin('interiores as i', 'i.vehiculos_id', '=', 'v.id')
                        ->leftJoin('exteriores as e', 'e.vehiculos_id', '=', 'v.id')->where('v.id', '=', $request->vehiculos_id)
                        ->select('v.img as vehiculo', 'i.interiores as interior', 'e.exteriores as exterior')->get();
        for ($i = 0; $i < count($listDrop); $i++) {
            if ($listDrop[$i]->vehiculo != '' || $listDrop[$i]->vehiculo != null) {
                unlink($listDrop[$i]->vehiculo);
            }
            if ($listDrop[$i]->interior != '' || $listDrop[$i]->interior != null) {
                unlink($listDrop[$i]->interior);
            }
            if ($listDrop[$i]->exterior != '' || $listDrop[$i]->exterior != null) {
                unlink($listDrop[$i]->exterior);
            }
        } */
        /* Consulta sql de Eliminacion */
        $delete = DB::table('vehiculos')->where('id', '=', $request->vehiculos_id)->delete();
        return 'Eliminado correctamente';
    }

    /* Editar */
    /* Visata de editar */

    public function view_editVehiculos($id = 0) {
        $categoria = DB::table('inventario_vehiculos')->get();
        $vehiculo = DB::table('vehiculos')->where('id', '=', $id)->first();
        $interiores = DB::table('interiores')->where('vehiculos_id', '=', $id)->get();
        $exteriores = DB::table('exteriores')->where('vehiculos_id', '=', $id)->get();
//        dd($exteriores);
//        return $id;
        return view('administracion.panel.panel_vehiculos_edit', compact(['categoria', 'vehiculo', 'interiores', 'exteriores']));
    }

    /**/

    public function editVehiculosDeleteIE(Request $request) {
        if ($request->ajax()) {
            /* Eliminar la img del directorio */
            if ($request->ie == 'interior') {
                $deleteInt = DB::table('interiores')->where('id', '=', $request->idIE)->first();
                if ($deleteInt->interiores != '' || $deleteInt->interiores != null) {
                    unlink($deleteInt->interiores);
                }
                $deleteInt = DB::table('interiores')->where('id', '=', $request->idIE)->delete();
            } else {
                $deleteExt = DB::table('exteriores')->where('id', '=', $request->idIE)->first();
                if ($deleteExt->exteriores != '' || $deleteExt->exteriores != null) {
                    unlink($deleteExt->exteriores);
                }
                $deleteExt = DB::table('exteriores')->where('id', '=', $request->idIE)->delete();
            }
            return 'Eliminado correctamente';
        }
    }

    public function editVehiculosAddIE(Request $request) {
        /* insertar en las dos opciones */
         if (is_array($request->int_img)) {
            $indice = array_keys($request->int_img);
            for ($j = 0; $j < count($indice); $j++) {
                if (file($request->int_img[$indice[$j]])) {
                    $file_int = $request->int_img[$indice[$j]];
                    $name_int = 'int_' . $request->idVehiculo . '_' . rand() . $file_int->getClientOriginalName();
                    $file_int->move(public_path() . '/images/BD_imagenes/', $name_int);
                    DB::table('interiores')->insert([
                    'vehiculos_id' => $request->idVehiculo,
                    'interiores' => 'images/BD_imagenes/' . $name_int,
                    'descripcion' => ($request->int_desc[$indice[$j]] == "" || $request->int_desc[$indice[$j]] == null) ? 'NO' : $request->int_desc[$indice[$j]],
                ]);
                }
            }
        }
          if (is_array($request->ext_img)) {
            $indice_ext = array_keys($request->ext_img);
            for ($j = 0; $j < count($indice_ext); $j++) {
                if (file($request->ext_img[$indice_ext[$j]])) {
                    $file_ext = $request->ext_img[$indice_ext[$j]];
                    $name_ext = 'ext_' . $request->idVehiculo . '_' . rand() . $file_ext->getClientOriginalName();
                    $file_ext->move(public_path() . '/images/BD_imagenes/', $name_ext);
                    DB::table('exteriores')->insert([
                    'vehiculos_id' => $request->idVehiculo,
                    'exteriores' => 'images/BD_imagenes/' . $name_ext,
                    'descripcion' => ($request->ext_desc[$indice_ext[$j]] == "" || $request->ext_desc[$indice_ext[$j]] == null) ? 'NO' : $request->ext_desc[$indice_ext[$j]],
                ]);
                }
            }
        }
        return redirect()->route('panel_vehiculos_editView', ['id' => $request->idVehiculo]);
    }

    public function editVehiculos(Request $request) {
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $name = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/images/BD_imagenes/', $name);
            /* Validar si la imagen cambio */
            $this->removeBeforeImgVehiculo($request->idVehiculo);
            DB::table('vehiculos')->where('id', '=', $request->idVehiculo)->update([
                'img' => 'images/BD_imagenes/' . $name,
                'nombre' => $request->nombre,
                'precio' => $request->precio,
                'descripcion' => $request->descripcion,
                'inv_id' => $request->categoria
            ]);
        }
        DB::table('vehiculos')->where('id', '=', $request->idVehiculo)->update([
            'nombre' => $request->nombre,
            'precio' => $request->precio,
            'descripcion' => $request->descripcion,
            'inv_id' => $request->categoria
        ]);

        //Interiores
        if (is_array($request->interior_desc)) {
            $indices_int = array_keys($request->interior_desc);
            for ($indices = 0; $indices < count($indices_int); $indices++) {
                /* Validar si interior_img es arreglo y si existe indice en exterior_img */
                if (is_array($request->interior_img)) {
                    if (array_key_exists($indices_int[$indices], $request->interior_img)) {
                        if (file($request->interior_img[$indices_int[$indices]])) {
                            $file_ext = $request->interior_img[$indices_int[$indices]];
                            $name_int = 'int_' . $request->idVehiculo . '_' . rand() . $file_ext->getClientOriginalName();
                            $file_ext->move(public_path() . '/images/BD_imagenes/', $name_int);
                            $this->removeBeforeInteriorImg($request->idVehiculo, $request->interior_id[$indices_int[$indices]]);
                            DB::table('interiores')->where('vehiculos_id', '=', $request->idVehiculo)->
                                    where('id', '=', $request->interior_id[$indices_int[$indices]])->update([
                                'interiores' => 'images/BD_imagenes/' . $name_int,
                                'descripcion' => ($request->interior_desc[$indices_int[$indices]]=='' || $request->interior_desc[$indices_int[$indices]]==null)? 'NO' : $request->interior_desc[$indices_int[$indices]]
                            ]);
                            continue;
                        }
                    }
                }
                /* Aqui va la actualizacion sin la img */
                DB::table('interiores')->where('vehiculos_id', '=', $request->idVehiculo)->
                        where('id', '=', $request->interior_id[$indices_int[$indices]])->update([
                    'descripcion' => ($request->interior_desc[$indices_int[$indices]]=='' || $request->interior_desc[$indices_int[$indices]]==null)? 'NO' : $request->interior_desc[$indices_int[$indices]]
                ]);
            }
        }
        /* Exteriores */
        if (is_array($request->exterior_desc)) {
            $indices_ext = array_keys($request->exterior_desc);
            for ($indices = 0; $indices < count($indices_ext); $indices++) {
                /* Validar si exterior_img es arreglo y si existe indice en exterior_img */
                if (is_array($request->exterior_img)) {
                    if (array_key_exists($indices_ext[$indices], $request->exterior_img)) {
                        if (file($request->exterior_img[$indices_ext[$indices]])) {
                            $file_ext = $request->exterior_img[$indices_ext[$indices]];
                            $name_ext = 'ext_' . $request->idVehiculo . '_' . rand() . $file_ext->getClientOriginalName();
                            $file_ext->move(public_path() . '/images/BD_imagenes/', $name_ext);
                            $this->removeBeforeExteriorImg($request->idVehiculo, $request->exterior_id[$indices_ext[$indices]]);
                            DB::table('exteriores')->where('vehiculos_id', '=', $request->idVehiculo)->
                                    where('id', '=', $request->exterior_id[$indices_ext[$indices]])->update([
                                'exteriores' => 'images/BD_imagenes/' . $name_ext,
                                'descripcion' => ($request->exterior_desc[$indices_ext[$indices]]==''|| $request->exterior_desc[$indices_ext[$indices]]==null)? 'NO': $request->exterior_desc[$indices_ext[$indices]]
                            ]);
                            continue;
                        }
                    }
                }
                /* Aqui va la actualizacion sin la img */
                DB::table('exteriores')->where('vehiculos_id', '=', $request->idVehiculo)->
                        where('id', '=', $request->exterior_id[$indices_ext[$indices]])->update([
                    'descripcion' => ($request->exterior_desc[$indices_ext[$indices]]==''|| $request->exterior_desc[$indices_ext[$indices]]==null)? 'NO': $request->exterior_desc[$indices_ext[$indices]]
                ]);
            }
        }
        return redirect()->route('panel');
    }

    /* Ext */

    public function removeBeforeImgVehiculo($idVehiculo) {
        $vehiculo = DB::table('vehiculos')->where('id', '=', $idVehiculo)->first();
        if ($vehiculo->img != '' || $vehiculos->img != null) {
            unlink($vehiculo->img);
            return "Eliminado";
        }
    }

    public function removeBeforeExteriorImg($idVehiculo, $idExterior) {
        $exteriores = DB::table('exteriores')->where('vehiculos_id', '=', $idVehiculo)->where('id', '=', $idExterior)->first();
        if ($exteriores->exteriores != '' || $exteriores->exteriores != null) {
            unlink($exteriores->exteriores);
            return "Eliminado";
        }
    }

    public function removeBeforeInteriorImg($idVehiculo, $idInterior) {
        $interiores = DB::table('interiores')->where('vehiculos_id', '=', $idVehiculo)->where('id', '=', $idInterior)->first();
        if ($interiores->interiores != '' || $interiores->interiores != null) {
            unlink($interiores->interiores);
            return "Eliminado";
        }
    }

}