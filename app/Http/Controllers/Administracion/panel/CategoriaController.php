<?php

namespace App\Http\Controllers\Administracion\panel;
use Illuminate\Http\Request;
use App\Categoria;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CategoriaController extends Controller
{
     /*Constructor*/
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(){
        $categorias= Categoria::all();
        return view('administracion.panel.panel_categoria')->with(compact('categorias'));
    }
    public function addCategoria(Request $request){
        if($request->ajax()){
          $cat=new Categoria();
          $cat->categoria=$request->categoria;
          $cat->save();
        return "Agregado Exitosamente";
        }
    }
    
    public function editCategoria(Request $request){
        if($request->ajax()){
            $cat= Categoria::find($request->idCat);
            $cat->categoria=$request->categoria;
            $cat->save();
        return "Modificado Exitosamente";
        }
    }
    
    public function deleteCategoria(Request $request){
        if($request->ajax()){
           $validar=DB::table('inventario_vehiculos as iv')->
                   join('vehiculos as v','v.inv_id','=','iv.id')->where('iv.id','=',$request->idCategoria)->get();
           if($validar->isEmpty()){
               Categoria::destroy($request->idCategoria);
               return "Se ha eliminado el registro";
           }else{
               return 'No se puede Borrar el registro';
           }
        }
    }
    
   
}
