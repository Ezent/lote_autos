<?php

namespace App\Http\Controllers\Administracion\panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
     /*Constructor*/
    public function __construct(){
        $this->middleware('auth');
    }
    public function index($id=0){
        $user= User::find($id);
        return view('administracion.panel.panel_user')->with(compact('user'));
    }
    
    public function editUser(Request $request){
        $user= User::find($request->idUser);
        $user->usuario=$request->usuario;
        $user->correo=$request->correo;
        $user->clave=$request->pass;
        $user->password= bcrypt($request->pass);
        $user->save();
        
        return redirect('/panel/categorias/user/'.request('idUser'))->with(['msj'=>'Actualizado correctamente']);
    }
}
