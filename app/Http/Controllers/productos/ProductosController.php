<?php

namespace App\Http\Controllers\productos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProductosController extends Controller {

    function index() {
        $img = $this->productos();
        return View('contenido.productos', ['img' => $img]);
    }

    /* Recursos de pruebas */

    function productos() {
        $producto = DB::table('vehiculos as v')->join('inventario_vehiculos as iv', 'iv.id', '=', 'v.inv_id')->
                        select('v.id as idVehiculo', 'v.img', 'v.nombre', 'v.precio', 'v.descripcion', 'iv.categoria')->get();
        $cat = DB::table('inventario_vehiculos as iv')->join('vehiculos as v', 'v.inv_id', '=', 'iv.id')
                ->groupBy('iv.id')->select('iv.categoria')->
                get();
        $msj='';
        if($producto->isEmpty()){
        $msj='No hay vehiculos en nuestro inventario!!!';
        return view('contenido.productos', ['producto' => $producto, 'cat' => $cat,'msj'=>$msj]);
        }
        return view('contenido.productos', ['producto' => $producto, 'cat' => $cat,'msj'=>$msj]);
    }

    public function viewVehiculos($id = 0) {
        $interiores= DB::table('interiores as i')->join('vehiculos as v','i.vehiculos_id','=','v.id')->where('v.id','=',$id)->
                select('i.interiores','i.descripcion')->get();
        $exteriores= DB::table('exteriores as e')->join('vehiculos as v','e.vehiculos_id','=','v.id')->where('v.id','=',$id)->
                select('e.exteriores','e.descripcion')->get();
        $vehiculo= DB::table('vehiculos as v')->where('v.id','=',$id)->first();
        if($interiores->isEmpty() && $exteriores->isEmpty()){
            return redirect()->route('productos');
        }
        return view('contenido/vista_vehiculos')->with(compact(['interiores','exteriores','vehiculo']));
//        return view('contenido/vista_vehiculos', ['auto' => $auto[($id - 1)], 'img' => $img]);
    }

}