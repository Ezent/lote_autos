<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table='inventario_vehiculos';
    
    protected $fillable=[
        'categoria'
    ];


    public $timestamps=false;
}
