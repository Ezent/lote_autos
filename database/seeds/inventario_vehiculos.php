<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class inventario_vehiculos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('inventario_vehiculos')->insert([
            'categoria' => 'AUTOS'
        ]);
          DB::table('inventario_vehiculos')->insert([
            'categoria' => 'PICK-UPS'
        ]);

          DB::table('inventario_vehiculos')->insert([
            'categoria' => 'CROSSOVER'
        ]);

          DB::table('inventario_vehiculos')->insert([
            'categoria' => 'COMERCIALES'
        ]);
           DB::table('inventario_vehiculos')->insert([
            'categoria' => 'MOTOS'
        ]);

    }
}
