<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class buscarSeeder extends Seeder {

    public function run() {
        DB::table('buscar')->insert([
            'url' => '/administrador',
            'nombre' => 'Login',
            'descripcion' => 'Solo para personal Administrativo',
        ]);
         DB::table('buscar')->insert([
            'url' => '/productos',
            'nombre' => 'Productos',
            'descripcion' => 'Conoce la gama de autos que tenemos para ti',
        ]);
          DB::table('buscar')->insert([
            'url' => '/about',
            'nombre' => '¿Quiénes somos?',
            'descripcion' => 'Todo acerca de nosotros',
        ]);
           DB::table('buscar')->insert([
            'url' => '/servicios',
            'nombre' => 'servicios',
            'descripcion' => 'Conoce todos nuestros servicios',
        ]);
            DB::table('buscar')->insert([
            'url' => '/contacto',
            'nombre' => 'contactos',
            'descripcion' => 'Acercate a nosotros o contactanos',
        ]);
            
    }

}
