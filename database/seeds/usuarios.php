<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class usuarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'usuario' => 'admin',
            'correo' => 'admin@gmail.com',
            'clave' => 'admin',
            'password' => bcrypt('admin')
        ]);
    }
}
