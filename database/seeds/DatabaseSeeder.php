<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**Aqui se agregan todos nuestros seeder para rellenar las bd**/
       $this->call(buscarSeeder::class);
       $this->call(inventario_vehiculos::class);
       $this->call(usuarios::class);
    }
}
