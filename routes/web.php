<?php
/*por mietras*/
//Route::get('/email', function () {
//    return view('email/email');
//});
Route::get('/carrusel', function () {
    return view('plantilla/carrusel');
});
Route::get('/','Administracion\panel\PublicacionesController@starPage')->name('start');
Route::get('/servicios', function () {
    return view('contenido.servicios');
});

Route::get('/ventas', function () {
    return view('contenido.ventas');
});
Route::get('/rentas', function () {
    return view('contenido.rentas');
});
Route::get('/consignaciones', function () {
    return view('contenido.consignaciones');
});
Route::get('/financiamiento', function () {
    return view('contenido.financiamiento');
});

Route::get('/contacto', function () {
    return view('contenido.contacto');
});

Route::get('/about', function () {
    return view('contenido.about');
});
Route::get('/productos', 'productos\ProductosController@productos')->name('productos');
Route::get('/vista_vehiculos/{id?}', 'productos\ProductosController@viewVehiculos')->name('vehiculos');
Route::get('/slider/post', 'Administracion\panel\PublicacionesController@slider')->name('slider');
Route::post('/slider/new', 'Administracion\panel\PublicacionesController@new')->name('slider_new');
Route::post('/slider/edit', 'Administracion\panel\PublicacionesController@edit')->name('slider_edit');
Route::post('/slider/delete', 'Administracion\panel\PublicacionesController@delete')->name('slider_delete');
Route::post('/buscar', 'Administracion\buscar\BuscarController@datos')->name('buscar');

/*Panel administrativo*/
Route::get('/panel','Administracion\panel\PanelController@index')->name('panel');
Route::get('/panel/vehiculos','Administracion\panel\PanelController@vehiculos')->name('panel_vehiculos');
Route::get('/panel/vehiculos/edit/{id?}','Administracion\panel\PanelController@view_editVehiculos')->name('panel_vehiculos_editView');
 
/*Peticiones ajax o por formulario*/
Route::post('/panel/vehiculos','Administracion\panel\PanelController@addVehiculos')->name('panel_vehiculos_add');
Route::post('/panel/vehiculos_view','Administracion\panel\PanelController@viewVehiculos')->name('panel_vehiculos_view');
Route::post('/panel/vehiculos_drop','Administracion\panel\PanelController@dropVehiculos')->name('panel_vehiculos_drop');

Route::post('/panel/vehiculos_edit','Administracion\panel\PanelController@editVehiculos')->name('panel_vehiculos_edit');
Route::post('/panel/vehiculos_edit/DIE','Administracion\panel\PanelController@editVehiculosDeleteIE')->name('panel_vehiculos_edit_delete_IE');
Route::post('/panel/vehiculos_e_a','Administracion\panel\PanelController@editVehiculosAddIE')->name('panel_vehiculos_edit_add_IE');

/*Panel administrativo categorias*/
Route::get('/panel/categorias','Administracion\panel\CategoriaController@index')->name('panel_categoria');

/*ajax panel_categoria*/
Route::post('/panel/categorias/editar','Administracion\panel\CategoriaController@editCategoria')->name('panel_categoria_edit');
Route::post('/panel/categorias/agregar','Administracion\panel\CategoriaController@addCategoria')->name('panel_categoria_add');
Route::post('/panel/categorias/delete','Administracion\panel\CategoriaController@deleteCategoria')->name('panel_categoria_delete');

/*Panel administrativo users*/
Route::get('/panel/categorias/user/{id}','Administracion\panel\UserController@index')->name('panel_user');
Route::post('/panel/categorias/user_edit','Administracion\panel\UserController@editUser')->name('panel_user_edit');


/*Logueo*/
Route::get('/administrador','Administracion\panel\LoginController@index')->name('startLogin')->middleware('guest');
Route::post('/administrador/login','Administracion\panel\LoginController@panel_login')->name('panel_login');
Route::get('/administrador/logout','Administracion\panel\LoginController@panel_logout')->name('panel_logout');

/*Ruta para enviar mail*/
Route::post('/send/email','mail\MailController@mailSend')->name('sendEmail');
?>