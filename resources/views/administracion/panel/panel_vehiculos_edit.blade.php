@extends('administracion.plantilla.plantilla_administracion')
@section('contenido')
@if($errors->any())
<h1 class="alert-danger"> A ocurrido un error interno, recarga la pagina o inicia de nuevo la aplicacion</h1>
<p class="alert-warning">Problemas tecnicos: consulta con el administrador</p>
@endif
<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <!-- /.panel-heading -->
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">
                        @if(session('msj'))
                        <div id="msj" class="{{(session('msj')=='Editado correctamente')? 'alert alert-success' : 'alert alert-danger'}}" >{{session('msj')}}</div>
                        @endif
                        <form action="{{route('panel_vehiculos_edit')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden"  name="idVehiculo" value="{{$vehiculo->id}}">
                            <fieldset>
                                <legend class="text-center header"><h3>Editar Vehiculos</h3></legend>
                                <div class="form-group">
                                    <span class="col-md-3 col-md-offset-9">[<i  class="text-danger">*</i>] Campos requeridos</span>
                                </div>

                                <div class="form-group">
                                    <span class="col-md-2 col-md-offset-1 text-right"><i class="fa fa-truck bigicon">
                                        </i> Categoria: <i  class="text-danger">*</i> </span>
                                    <div class="col-md-8">
                                        <select required name="categoria" class="form-control">
                                            <option></option>
                                            @foreach($categoria as $item)
                                            <option value="{{$item->id}}" {{($vehiculo->inv_id==$item->id) ? 'selected' : ''}}>
                                                {{$item->categoria}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-2 col-md-offset-1 text-right"><i class="fa fa-car bigicon"></i> 
                                        Nombre y Modelo: <i  class="text-danger">*</i> 
                                    </span>
                                    <div class="col-md-8">
                                        <input required id="lname" name="nombre" type="text" placeholder="Ej. Audi RS Q3 Performance" class="form-control" value="{{$vehiculo->nombre}}">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <span class="col-md-2 col-md-offset-1 text-right"><i class="fa fa-car bigicon"></i> <i class="fa fa-money bigicon"></i> 
                                        Precio: <i  class="text-danger">*</i> 
                                    </span>
                                    <div class="col-md-8">
                                        <input required id="lname" name="precio" type="text" placeholder="" class="form-control" value="{{$vehiculo->precio}}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span class="col-md-2 col-md-offset-1 text-right"><i class="fa fa-automobile bigicon"></i> <i class="fa fa-pencil bigicon"></i>
                                        Descripcion:</span>
                                    <div class="col-md-8">
                                        <input name="descripcion" type="text" placeholder="Descripcion del automovil" class="form-control" value="{{$vehiculo->descripcion}}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span class="col-md-2 col-md-offset-1 text-right"><i class="fa fa-picture-o bigicon">

                                        </i> Imagen: <i  class="text-danger">*</i> </span>
                                    <div class="col-md-8">
                                        <!--Preview-->
                                        <div id="uploadForm">
                                            <input type="file" name="img" id="file" class="form-control" />
                                            <embed id="previewStart" class="img-thumbnail" src="/{{$vehiculo->img}}" width="120" height="60">
                                        </div>
                                    </div>
                                </div>
                                @if(!$interiores->isEmpty() || !$exteriores->isEmpty())
                                <div>
                                    <h2 class="col-md-7 col-md-offset-3"><p align='center'>Interiores/Exteriores</p></h2>
                                    <div class="row col-md-8  col-md-offset-3">
                                        <table class="table table-responsive">
                                            <thead>
                                                <tr>
                                                    <th><p align='center'>Descripcion</p></th>
                                                    <th><p align='center'>Seleccion</p></th>
                                                    <th><p align='center'>Preview</p></th>
                                                    <th><p align='center'>Quitar</p></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!$interiores->isEmpty())
                                                <tr style="background: white;"><td colspan="4"><p align='center'>Interiores</p></td></tr>
                                                @endif
                                                @foreach($interiores as $item)
                                                <tr>
                                                    <!--id oculto -->
                                            <input  type="hidden"  name="interior_id[]" value="{{$item->id}}"/>
                                            <td><p align='center'>
                                                <input   class="form-control" name="interior_desc[]" value="{{($item->descripcion=='NO')?'': $item->descripcion}}" placeholder="Sin descripcion ..." /><p></td>
                                            <td> 
                                                <input onchange="loadFile(event,'int{{$item->id}}')" type="file" name="interior_img[]" class="form-control"/>
                                            </td>
                                            <td>
                                                <p align='center'>
                                                    <img id="int{{$item->id}}" class="img-thumbnail" src="/{{$item->interiores}}" width="140" height="50"> 
                                                </p>
                                            </td>
                                            <td><a onclick="deleteIE('{{$item->id}}', 'interior')" class="btn btn-default btn-circle"> &timesb;</a></td>
                                            </tr>
                                            @endforeach
                                            @if(!$exteriores->isEmpty())
                                            <tr style="background: white;"><td colspan="4"><p align='center'>Exteriores</p></td></tr>
                                            @endif
                                            @foreach($exteriores as $item)
                                            <tr>
                                                         <!--id oculto -->
                                            <input  type="hidden"  name="exterior_id[]" value="{{$item->id}}"/>
                                                <td><p align='center'><input class="form-control" name="exterior_desc[]" value="{{($item->descripcion=='NO')?'': $item->descripcion}}" placeholder="Sin descripcion ..."  /><p></td>
                                                <td> 
                                                    <input type="file" name="exterior_img[]" class="ext form-control" onchange="loadFile(event,'ext{{$item->id}}')"/>
                                                </td>
                                                <td>
                                                    <p align='center'>
                                                        <img id="ext{{$item->id}}" class="img-thumbnail" src="/{{$item->exteriores}}" width="140" height="50"> 
                                                    </p>
                                                </td>
                                                <td><a onclick="deleteIE('{{$item->id}}', 'exterior')" class="btn btn-default btn-circle"> &timesb;</a></td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @else
                                <div class="alert-danger">Este vehiculo no tiene imagenes que describan sus interiores y exteriores</div>
                                @endif
                                <hr>
                                <div class="form-group">
                                    <div class="col-md-12" style="height: 50px;"></div>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary btn-lg">Aceptar</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <h1>Añadir Interiores</h1>
                    <!-- Boton para mostrar interiores y exteriores-->
                    <button class="btn btn-secondary" onclick="return showHide('ocultable')" type="button">Interiores / Exteriores</button>
                    <!--Aqui va nurestro opcionales -->
                    <hr>
                    <div id="ocultable" style="display: none;">
                        <form id="formaddIE" action="{{route('panel_vehiculos_edit_add_IE')}}"  method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden"  name="idVehiculo" value="{{$vehiculo->id}}">
                            <div class="col-md-6">
                                <h4 class="text-center">Interiores</h4>
                                <fieldset id="foto_interior"><legend></legend>
                                    <div>
                                        <span class="col-md-3 text-right"><i class="fa fa-picture-o bigicon">
                                            </i> Imagen: </span>
                                        <div class="col-md-8">
                                            <input id="phone" name="int_img[]" type="file"  class="form-control">
                                        </div>
                                    </div>
                                    <div>
                                        <span class="col-md-3 text-right"><i class="fa fa-pencil bigicon">
                                            </i> Descripción: </span>
                                        <div class="col-md-8">
                                            <textarea name="int_desc[]" placeholder="Escibe una decripcion de la imagen" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </fieldset>
                                <div id="clon_Interior" ></div>
                                <button type="button"  onclick="addNode(document.querySelector('#foto_interior'), 'clon_Interior', 'clonInterior')" class="btn btn-success "><i class="fa fa-plus"></i></button>
                                <button type="button" onclick="removeNode('.clonInterior')" class="btn btn-info"><i class="fa fa-minus"></i></button>

                            </div>
                            <div class="col-md-6">
                                <h4 class="text-center">Exteriores</h4>
                                <fieldset id="foto_exterior"><legend></legend>
                                    <div>
                                        <span class="col-md-3 text-right"><i class="fa fa-picture-o bigicon">
                                            </i> Imagen: </span>
                                        <div class="col-md-8">
                                            <input name="ext_img[]" type="file"  class="form-control">
                                        </div>
                                    </div>
                                    <div>
                                        <span class="col-md-3 text-right"><i class="fa fa-pencil bigicon">
                                            </i> Descripción: </span>
                                        <div class="col-md-8">
                                            <textarea name="ext_desc[]" placeholder="Escibe una decripcion de la imagen" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </fieldset>
                                <div id="clon_exterior"></div>
                                <div class="pull-right">
                                    <button type="button"  onclick="addNode(document.querySelector('#foto_exterior'), 'clon_exterior', 'clonExterior')" class="btn btn-success "><i class="fa fa-plus"></i></button>
                                    <button type="button" onclick="removeNode('.clonExterior')" class="btn btn-info"><i class="fa fa-minus"></i></button>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12" style="height: 50px;"></div>
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">Añadir</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<!--tokens -->
@csrf
@endsection
@section('js')
<script>
    function showHide(id) {
    var elemento = document.getElementById(id);
    if (!elemento) {
    return true;
    }
    if (elemento.style.display == "none") {
    elemento.style.display = "block"
    } else {
    elemento.style.display = "none"
    }
    ;
    return true;
    }

    /*Para la clonacion de nodos*/
    var cont = 1

            function addNode(nodeClon, nodeDady, className) {
            var nodeFather = document.getElementById(nodeDady);
            var nodeSon = nodeClon.cloneNode(true);
            nodeSon.childNodes[2].childNodes[3].childNodes[1].value = null;
            nodeSon.childNodes[4].childNodes[3].childNodes[1].value = null;
            nodeSon.className = className;
            nodeFather.appendChild(nodeSon);
            cont = cont + 1;
            }

    function removeNode(className) {
    var nodes_delete = document.querySelectorAll(className);
    if (nodes_delete.length > 0) {
    var node_delete = nodes_delete[nodes_delete.length - 1];
    node_delete.parentNode.removeChild(node_delete);
    cont = cont - 1;
    }
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
    setTimeout(function () {
    $("#msj").fadeOut(1500);
    }, 4000);
    });</script>

<!--Otros escript -->
<script>
    $("#file").change(function () {
    filePreview(this);
    });
    function filePreview(input) {
    if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
    $('#previewStart').remove();
    $('#uploadForm + embed').remove();
//                $('#previewImg').src=e.target.result;
    $('#uploadForm').after('<embed class="img-thumbnail" src="' + e.target.result + '" width="120" height="60">');
    }
    reader.readAsDataURL(input.files[0]);
    }
    }
    /*Iteriores y extereiores mostrar preview*/
    var loadFile = function(event, idOutput) {
    var output = document.getElementById(idOutput);
    output.src = URL.createObjectURL(event.target.files[0]);
    };
    /*funcion ajax para quitar IE*/
    function deleteIE(id, tipo){
    var token = document.getElementsByName('_token');
    /*Enviar peticion*/
    var data = {idIE:id, _token:token[0].value, ie:tipo};
    var url = '{{route('panel_vehiculos_edit_delete_IE')}}';
    $.post(url, data, function(msj){
    alert(msj);
    location.reload(true);
    });
    }
    function addIE(){
    var form = $('#formaddIE');
    var url = form.attr('action');
    var data = form.serialize();
    alert(data);
    $.post(url, data, function(msj){
    alert(msj);
    });
    }
</script>
@endsection