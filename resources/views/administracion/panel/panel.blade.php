@extends('administracion.plantilla.plantilla_administracion')
@section('carrusel')
@endsection
@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de los vehiculos en existencia
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <!--<div class="table-responsive ">-->
                    <table width="100%" class="table table-striped table-bordered table-hover " id="dataTables-example">
                        <!-- recorrer variable -->
                        <thead>
                            <tr>
                                <th><p align='center'>Imagen</p></th>
                                <th><p align='center'>Categoria</p></th>
                                <th><p align='center'>Vehiculo/Modelo</p></th>
                                <th><p align='center'>Precio</p></th>
                                <th><p align='center'>Descripcion(s)</p></th>
                                <th><p align='center'>Fecha lanzamiento</p></th>
                                <th><p align='center'>Acciones</p></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($datos as $item)
                            <tr class="odd gradeX">
                                <td><p align='center'><img src="{{$item->img}}" class='img-responsive img-circle' style="height: 10%; width: 30%;" /></p></td>
                                <td><p align='center'>{{$item->categoria}}</p></td>
                                <td><p align='center'>{{$item->nombre}}</p></td>
                                <td><p align='center'>{{$item->precio}}</p></td>
                                <td><p align='center'>{{$item->descripcion}}</p></td>
                                <td><p align='center'>{{$item->registro}}</p></td>
                                <td><div style="display: flex;  top: 50%;">
                                        <div><a class="btn btn-success btn-xs" onclick="printDescription('{{$item->id_vehiculos}}')">Ver</a></div>
                                        <div><a  class="btn btn-warning btn-xs" href="{{url('/panel/vehiculos/edit').'/'.$item->id_vehiculos}}">Editar</a></div>
                                        <div><a onclick="pasarId('{{$item->id_vehiculos}}')" class="btn btn-danger btn-xs" >Eliminar</a></div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    <!--                        </div>-->
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>

<!--Modal  -->
<div class="container jumbotron" style="display: none;">
    <button id="eventoModal" type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-2">Modal 2</button>
</div>
<div class="modal fade" id="modal-2">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detalles del Vehiculo</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4"><h3><p align='center'>AUTO</p></h3></div>
                    </div>
                    <div class="row">
                        <div id="vehiculo"></div>
                    </div>
                    <div class="row">
                        <div id="textInt"></div><!-- text Interiores-->

                    </div>
                    <div class="row">
                        <div  id="vehiculo_interior"></div><!--Interiores-->
                    </div>
                    <div class="row">
                        <div id="textExt"></div><!-- text Exteriores -->
                    </div>
                    <div class="row">
                        <div  id="vehiculo_exterior"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" href="#modal-1" data-toggle="modal" data-dismiss="modal">Cerrar</a>
            </div>
        </div>
    </div></div>
<!--Fin de modal -->
<!--Modal Eliminar -->
<div class="container jumbotron" style="display: none;">
    <button id="delete" type="button" class="btn btn-info" data-toggle="modal" data-target="#modalDrop">Delete</button>
</div>
<div class="modal fade" id="modalDrop">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" style="background: #D34240;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="color: white"> <b>Eliminar</b> vehiculo <span><i class="fa fa-car"></i><i class="icon icon-delete"></i></span></h3>
            </div>
            <div class="modal-body">
                 <h4>Seguro de <b>Eliminar</b> el vehiculo? </h4>
            </div>
            <div class="modal-footer">
                 <!--Formulario -->
                 <form id="formDelete" action="{{route('panel_vehiculos_drop')}}"  onsubmit="deleteVehiculo(); return false;" method="post">
                     @csrf
                     <input  id="idDelete" type="hidden" name="vehiculos_id"/>
                     <button type="submit" class="btn btn-warning" >SI</button>
                 <a id="closeWindow" class="btn btn-success" data-toggle="modal" data-dismiss="modal">No</a>
                </form>
                 
            </div>
        </div>
    </div></div>
<!--Fin Modal -->
@csrf
<!-- script-->
<script type="text/javascript">
    /*Eliminar*/
/*Pasar id a eliminar*/
function pasarId(id){
    var idField=document.getElementById('idDelete');
    idField.value=id;
    var modal=document.getElementById('delete');
    modal.click();
}
/*Eliminar registro refrescar la pagina*/
 function deleteVehiculo(){
     /*boton para cerrar modal despues del evento*/
     var closeWindow=document.getElementById('closeWindow');
     var id=document.getElementById('idDelete');
     var form=$("#formDelete");
     var url=form.attr('action');
     var data=form.serialize();
    $.post(url, data, function(msj){
        alert(msj);
        location.reload(true);
    });
    closeWindow.click();
}

    function printDescription(idVehiculo){
    /*Clic eventoModal*/
    var evento = document.getElementById('eventoModal');
    var interior = document.getElementById('showInterior');
    var token = document.getElementsByName('_token');
    /*Enviar peticion*/
    var data = {id:idVehiculo, _token:token[0].value};
    var url = '{{route('panel_vehiculos_view')}}';
    $.post(url, data, function(msj){ /*starAjax*/
    /*Map Interiores*/
    var imgFirst_interior = msj['interiores'][0];
    fotoPrincipal = '<div class="col-md-4 col-md-offset-4"><p align="center">' +
            '<img  src="' +msj['vehiculo'].img+ '" width="50%"/></p></div>';
    /*poner la imagen*/
    document.getElementById('vehiculo').innerHTML = fotoPrincipal;
    /*Interiores*/
    var interiores = msj['interiores'].map(function (bar){
    return '<div class="col-md-4"><p align="center"><img  src="' + bar.interiores + '"  width="50%"/></p></div>'
            });
    var textInt = '<hr><div class="col-md-4 "><h3><p align="center">Interiores</p></h3></div>';
    var int = /,/g;
    interiores = interiores.toString().replace(int, '');
    /*poner img´s interiors*/
    document.getElementById('textInt').innerHTML = (interiores != '') ? textInt : '';
    document.getElementById('vehiculo_interior').innerHTML = interiores;
    /*Exteriores*/
    var exteriores = msj['exteriores'].map(function (bar){
    return '<div class="col-md-4"><p align="center"><img  src="' + bar.exteriores + '"  width="50%"/></p></div>'
            });
    var textExt = '<hr><div class="col-md-4"><h3><p align="center">Exteriores</p></h3></div>';
    var ext = /,/g;
    exteriores = exteriores.toString().replace(ext, '');
    /*poner img´s interiors*/
    document.getElementById('textExt').innerHTML = (exteriores != '') ? textExt : '';
    document.getElementById('vehiculo_exterior').innerHTML = exteriores;
    evento.click();
    }); /*endAjax*/
    }
</script>
<!-- fin script-->
@endsection
