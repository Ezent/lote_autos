@extends('administracion.plantilla.plantilla_administracion')
@section('carrusel')
@endsection
@section('contenido')
<div class="container">
    <div class="row mt">
        <!--  DATE PICKERS -->
        <!--Mensaje de actualizacion -->
        @if(session('msj'))
        <div id="msj" class="alert alert-success">{{session('msj')}}</div>
        @endif
        <div class="col-lg-12">
            <div class="form-panel" >
                <form action="{{route('panel_user_edit')}}" class="form-horizontal style-form" method="post" >
                    @csrf
                    <!-- Campo oculto-->
                    <input value="{{auth()->user()->id}}" name="idUser" type="hidden">
                    <div class="form-group">
                        <label class="control-label col-md-3">Usuario :</label>
                        <div class="col-md-3 col-xs-11">
                            <input value="{{$user['usuario']}}" name="usuario" class="form-control form-control-inline input-medium default-date-picker" type="text" >
                            <span class="help-block">Escribe un alias que te identifique</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Correo :</label>
                        <div class="col-md-3 col-xs-11">
                            <input value="{{$user['correo']}}"  type="text" name="correo"  class="form-control">
                            <span class="help-block">Escribe un correo valido</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Password</label>
                        <div class="col-md-3 col-xs-11">
                            <input onblur="restaurarInput()" onfocus="cambiarInput()" id="p" value="{{$user['clave']}}"type="password" name="pass" class="form-control">
                            <span class="help-block">Escribe una contraseña segura</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"></label>
                        <div class="col-md-4">
                            <button  class="btn btn-default" type='submit'>Actualizar</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /form-panel -->
        </div>
        <!-- /col-lg-12 -->
    </div>
</div>
<script type="text/javascript">
    function cambiarInput(){
        var pass=document.getElementById('p');
        pass.type="text";
    }
    function restaurarInput(){
        var pass=document.getElementById('p');
        pass.type="password";
    }
    
    $(document).ready(function () {
    setTimeout(function () {
    $("#msj").fadeOut(1500);
    }, 3000);
    });
</script>
@endsection
