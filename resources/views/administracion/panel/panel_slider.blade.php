@extends('administracion.plantilla.plantilla_administracion') 
@section('carrusel')
@endsection
 
@section('contenido')
<div class="container">
    @if (session('msj'))
    <div id="msj" class="alert alert-warning">
        {{session('msj')}}
    </div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" type="button" class="btn btn-info" data-toggle="modal" data-target="#modalAdd">
                Agregar nueva</a>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>
                        <p align="center">Imagenes de publicidad de Inicio</p>
                    </h3>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <!--<div class="table-responsive ">-->
                    <table width="100%" class="table table-striped table-bordered table-hover " id="dataTables-example">
                        <!-- recorrer variable -->
                        <thead>
                            <tr>
                                <th>
                                    <p align='center'>Imagen</p>
                                </th>
                                <th>
                                    <p align='center'>Descripción</p>
                                </th>
                                <th>
                                    <p align='center'>Fecha lanzamiento</p>
                                </th>
                                <th>
                                    <p align='center'>Acciones</p>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($slider as $item)
                            <tr class="odd gradeX">
                                <td>
                                    <p align="center">
                                        <img class="img-rounded" width="100px" src="/{{$item->imagen}}">
                                    </p>
                                </td>
                                <td>
                                    <p align='center'>{{$item->descripcion}}</p>
                                </td>
                                <td>
                                    <p align='center'>{{$item->registro}}</p>
                                </td>
                                <td>
                                    <div>
                                        <p align='center'>
                                            <a class="btn btn-warning btn-xs" onclick="edit({{json_encode($item)}})" data-toggle="modal" data-target="#modalEdit">Editar</a>
                                            <a onclick="deletePost({{json_encode($item)}})" class="btn btn-danger btn-xs" title="Eliminar la publicación">Eliminar</a>
                                        </p>
                                    </div>
                                </td>

                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    <!--                        </div>-->
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<div class="container jumbotron" style="display: none;">
    <button id="delete" type="button" class="btn btn-info" data-toggle="modal" data-target="#modalDrop">Delete</button>
</div>
<div class="modal fade" id="modalDrop">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" style="background: #D34240;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="color: white"> <b>Eliminar</b> Categoria <span><i class="icon icon-delete"></i></span></h3>
            </div>
            <div class="modal-body">
                <h4>Seguro de <b>Eliminar</b> esta categoría? </h4>
            </div>
            <div class="modal-footer">
                <!--Formulario -->
                <form id="formDelete" action="{{route('slider_delete')}}" method="post">
                    @csrf
                    <input id="idDelete" type="hidden" name="id" />
                    <input id="anterior_delete" type="hidden" name="img_anterior_delete" />
                    <button type="submit" class="btn btn-warning">SI</button>
                    <a id="closeWindow" class="btn btn-success" data-toggle="modal" data-dismiss="modal">No</a>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Fin Modal -->

<!--star Modal Añadir -->
<div class="modal fade" id="modalAdd">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header" style="background: #5DA43D;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="color: white"> Añadir nueva </h3>
            </div>
            <div class="modal-body">
                <form action="{{route('slider_new')}}" enctype="multipart/form-data" method="POST">
                    @csrf
                    <label for="img">Imagen</label>
                    <input name="img" onchange="verImg(event,'preview')" id="img" type="file" class="form-control" required="true">
                    <!--Area mostrar img -->
                    <img id="preview" src="#" class="img-rounded" width="20%" style="display: none" />
                    <br>
                    <!-- fin area de mostrar imagen-->
                    <label for="desc">Descripcion:</label>
                    <input name="descripcion" type="text" class="form-control" placeholder="Escribe una Descripcion">
            </div>
            <div class="modal-footer">
                <!--Formulario -->
                <button type="submit" class="btn btn-warning">Aceptar</button>
                <a id="closeWindow" class="btn btn-success" data-toggle="modal" data-dismiss="modal">No</a>
                </form>

            </div>
        </div>
    </div>
</div>
<!--End modal Añadir -->
<!-- modal editar-->
<div class="modal fade" id="modalEdit">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header" style="background: #DCAF47;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="color: white"> Editar </h3>
            </div>
            <div class="modal-body">
                <form action="{{route('slider_edit')}}" enctype="multipart/form-data" method="POST">
                    @csrf
                    <!-- Img anterior-->
                    <input type="hidden" id="id" name="id" value="#">
                    <input type="hidden" id="anterior_img" name="anterior_img" value="#">
                    <div class="image-upload">
                        <label>Clic aqui para subir tu imagen --></label>
                        <label for="file-input">
                                <img style="cursor: pointer" id="preview2" src="#" alt ="Click aquí para subir tu foto" title ="Click aquí para subir tu foto" width="100px" class="img-rounded" > 
                            </label>
                        <input onchange="verImg(event,'preview2')" style="display: none" id="file-input" name="img" type="file" />
                    </div>
                    <br>
                    <!-- fin area de mostrar imagen-->
                    <label for="desc">Descripcion:</label>
                    <input id="edit_desc" name="descripcion" type="text" class="form-control" placeholder="Escribe una Descripcion">
            </div>
            <div class="modal-footer">
                <!--Formulario -->
                <button type="submit" class="btn btn-warning">Aceptar</button>
                <a id="closeWindow" class="btn btn-success" data-toggle="modal" data-dismiss="modal">No</a>
                </form>

            </div>
        </div>
    </div>
</div>
<!--  fin del modal -->
@csrf
<script type="text/javascript">
    /*Previsualizar imagen*/
    function verImg(event,img){
        var imgTag=document.getElementById(img);
        if(img=='preview'){
            imgTag.style.display="block";
        }
        imgTag.src=URL.createObjectURL(event.target.files[0]);
    }
    /*Editar*/
    function edit(obj) {
        var img=document.getElementById('preview2');
        var desc=document.getElementById('edit_desc');
        var ant_img=document.getElementById('anterior_img');
        var id=document.getElementById('id');
        img.src='/'+obj.imagen;
        desc.value=obj.descripcion;
        ant_img.value=obj.imagen;
        id.value=obj.id;
    }
   
    /*Eliminar*/
    function deletePost(obj) {
        var idField = document.getElementById('idDelete');
        var ant_del = document.getElementById('anterior_delete');
        idField.value = obj.id;
        ant_del.value=obj.imagen;
        var modal = document.getElementById('delete');
        modal.click();
        }
        $(document).ready(function () {
            setTimeout(function () {
            $("#msj").fadeOut(1500);
            }, 4000);
            });

</script>
@endsection