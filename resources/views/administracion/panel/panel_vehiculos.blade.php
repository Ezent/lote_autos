@extends('administracion.plantilla.plantilla_administracion')
@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <!-- /.panel-heading -->
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm">
                        @if(session('msj'))
                        <div id="msj" class="{{(session('msj')=='Insertado correctamente')? 'alert alert-success' : 'alert alert-danger'}}" >{{session('msj')}}</div>
                        @endif
                        <form action="{{route('panel_vehiculos_add')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                            @csrf
                            <fieldset>
                                <legend class="text-center header"><h3>Añadir Vehiculos</h3></legend>
                                <div class="form-group">
                                    <span class="col-md-3 col-md-offset-9">[<i  class="text-danger">*</i>] Campos requeridos</span>
                                </div>

                                <div class="form-group">
                                    <span class="col-md-2 col-md-offset-1 text-right"><i class="fa fa-truck bigicon">
                                        </i> Categoria: <i  class="text-danger">*</i> </span>
                                    <div class="col-md-8">
                                        <select required name="categoria" class="form-control">
                                            <option></option>
                                            @foreach($categoria as $item)
                                            <option value="{{$item->id}}">{{$item->categoria}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-2 col-md-offset-1 text-right"><i class="fa fa-car bigicon"></i> 
                                        Nombre y Modelo: <i  class="text-danger">*</i> 
                                    </span>
                                    <div class="col-md-8">
                                        <input required id="lname" name="nombre" type="text" placeholder="Ej. Audi RS Q3 Performance" class="form-control">
                                    </div>
                                </div>
                                  <div class="form-group">
                                    <span class="col-md-2 col-md-offset-1 text-right"><i class="fa fa-car bigicon"></i> <i class="fa fa-money bigicon"></i> 
                                        Precio: <i  class="text-danger">*</i> 
                                    </span>
                                    <div class="col-md-8">
                                        <input required id="lname" name="precio" type="text" placeholder="Ej.$ 250 000 mxn " class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span class="col-md-2 col-md-offset-1 text-right"><i class="fa fa-automobile bigicon"></i> <i class="fa fa-pencil bigicon"></i>
                                        Descripcion:</span>
                                    <div class="col-md-8">
                                        <input name="descripcion" type="text" placeholder="Descripcion del automovil" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span class="col-md-2 col-md-offset-1 text-right"><i class="fa fa-picture-o bigicon">

                                        </i> Imagen: <i  class="text-danger">*</i> </span>
                                    <div class="col-md-8">
                                        <input required  name="img" type="file"  class="form-control">
                                    </div>
                                </div>
                                <!-- Boton para mostrar interiores y exteriores-->
                                <button class="btn btn-secondary" onclick="return showHide('ocultable')" type="button">Interiores / Exteriores</button>
                                <!--Aqui va nurestro opcionales -->
                                <hr>
                                <div id="ocultable" style="display: none;">
                                    <div class="col-md-6">
                                        <h4 class="text-center">Interiores</h4>
                                        <fieldset id="foto_interior"><legend></legend>
                                            <div>
                                                <span class="col-md-3 text-right"><i class="fa fa-picture-o bigicon">
                                                    </i> Imagen: </span>
                                                <div class="col-md-8">
                                                    <input id="phone" name="interior_img[]" type="file"  class="form-control">
                                                </div>
                                            </div>
                                            <div>
                                                <span class="col-md-3 text-right"><i class="fa fa-pencil bigicon">
                                                    </i> Descripción: </span>
                                                <div class="col-md-8">
                                                    <textarea name="interior_desc[]" placeholder="Escibe una decripcion de la imagen" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div id="clon_Interior" ></div>
                                        <button type="button"  onclick="addNode(document.querySelector('#foto_interior'), 'clon_Interior', 'clonInterior')" class="btn btn-success "><i class="fa fa-plus"></i></button>
                                        <button type="button" onclick="removeNode('.clonInterior')" class="btn btn-info"><i class="fa fa-minus"></i></button>

                                    </div>
                                    <div class="col-md-6">
                                        <h4 class="text-center">Exteriores</h4>
                                        <fieldset id="foto_exterior"><legend></legend>
                                            <div>
                                                <span class="col-md-3 text-right"><i class="fa fa-picture-o bigicon">
                                                    </i> Imagen: </span>
                                                <div class="col-md-8">
                                                    <input name="exterior_img[]" type="file"  class="form-control">
                                                </div>
                                            </div>
                                            <div>
                                                <span class="col-md-3 text-right"><i class="fa fa-pencil bigicon">
                                                    </i> Descripción: </span>
                                                <div class="col-md-8">
                                                    <textarea name="exterior_desc[]" placeholder="Escibe una decripcion de la imagen" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div id="clon_exterior"></div>
                                        <div class="pull-right">
                                            <button type="button"  onclick="addNode(document.querySelector('#foto_exterior'), 'clon_exterior', 'clonExterior')" class="btn btn-success "><i class="fa fa-plus"></i></button>
                                            <button type="button" onclick="removeNode('.clonExterior')" class="btn btn-info"><i class="fa fa-minus"></i></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12" style="height: 50px;"></div>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary btn-lg">Aceptar</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    function showHide(id) {
        var elemento = document.getElementById(id);
        if (!elemento) {
            return true;
        }
        if (elemento.style.display == "none") {
            elemento.style.display = "block"
        } else {
            elemento.style.display = "none"
        }
        ;
        return true;
    }

    /*Para la clonacion de nodos*/
    var cont = 1

    function addNode(nodeClon, nodeDady, className) {
        var nodeFather = document.getElementById(nodeDady);
        var nodeSon = nodeClon.cloneNode(true);
        nodeSon.childNodes[2].childNodes[3].childNodes[1].value=null;
       nodeSon.childNodes[4].childNodes[3].childNodes[1].value=null;
        nodeSon.className = className;
        nodeFather.appendChild(nodeSon);
        cont = cont + 1;
    }

    function removeNode(className) {
        var nodes_delete = document.querySelectorAll(className);
        if (nodes_delete.length > 0) { 
            var node_delete = nodes_delete[nodes_delete.length - 1]; 
            node_delete.parentNode.removeChild(node_delete);
            cont = cont - 1;
        }
    }
</script>
<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $("#msj").fadeOut(1500);
    },4000);
});
</script>
@endsection