@extends('administracion.plantilla.plantilla_administracion')
@section('carrusel')
@endsection
@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <a onclick="add('{{route('panel_categoria_add')}}')" class="btn btn-success">Agregar nueva categoria</a>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3> <p align="center">Categorias</p></h3>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <!--<div class="table-responsive ">-->
                    <table width="100%" class="table table-striped table-bordered table-hover " id="dataTables-example">
                        <!-- recorrer variable -->
                        <thead>
                            <tr>
                                <th><p align='center'>Categoria</p></th>
                                <th><p align='center'>Fecha lanzamiento</p></th>
                                <th ><p align='center'>Acciones</p></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categorias as $item)
                            <tr class="odd gradeX">
                                <td><p align='center'>{{$item->categoria}}</p></td>
                                <td><p align='center'>{{$item->registro}}</p></td>
                                <td>
                                    <div >
                                        <p align='center'>
                                     <a  class="btn btn-warning btn-xs" onclick="edit({{json_encode($item)}},'{{route('panel_categoria_edit')}}')" >Editar</a>
                                    <a onclick="pasarId('{{$item->id}}')" class="btn btn-danger btn-xs" >Eliminar</a>
                                    </p>
                                    </div>
                                </td>

                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    <!--                        </div>-->
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>

<!--Modal  -->
<div class="container jumbotron" style="display: none;">
    <button id="eventoModal" type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-2"></button>
</div>
<div class="modal fade" id="modal-2">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header" style="">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 id="txtHeader" class="modal-title">Editar</h4>
            </div>
            <form id="formCat" action="#" onsubmit=" save(); return false;" method="post">
                <div class="modal-body">
                    @csrf
                    <input id="id" type="hidden" name="idCat">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <p>Categoria: </p>
                            </div>
                            <div class="col-md-8">
                                <input id="inputModal" class="form-control" type="text" name="categoria">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" >Guardar</button>
                    <button id="cerrarModal" class="btn btn-default" href="#modal-1" data-toggle="modal" data-dismiss="modal">Cancelar</button>
            </form>
        </div>
    </div>
</div></div>
<!--Fin de modal -->

<!--Modal Eliminar -->
<div class="container jumbotron" style="display: none;">
    <button id="delete" type="button" class="btn btn-info" data-toggle="modal" data-target="#modalDrop">Delete</button>
</div>
<div class="modal fade" id="modalDrop">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" style="background: #D34240;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="color: white"> <b>Eliminar</b> Categoria <span><i class="icon icon-delete"></i></span></h3>
            </div>
            <div class="modal-body">
                <h4>Seguro de <b>Eliminar</b> esta categoría? </h4>
            </div>
            <div class="modal-footer">
                <!--Formulario -->
                <form id="formDelete" action="{{route('panel_categoria_delete')}}" onsubmit="deleteCat();return false;"   method="post">
                    @csrf
                    <input  id="idDelete" type="hidden" name="idCategoria"/>
                    <button type="submit" class="btn btn-warning" >SI</button>
                    <a id="closeWindow" class="btn btn-success" data-toggle="modal" data-dismiss="modal">No</a>
                </form>

            </div>
        </div>
    </div></div>
<!--Fin Modal -->
@csrf
<!-- script-->
<script type="text/javascript">
    /*Editar*/
    function edit(obj, url) {
    var modal = document.getElementById('eventoModal');
    var input = document.getElementById('inputModal');
    var id = document.getElementById('id');
    var form = document.getElementById('formCat');
    var text = document.getElementById('txtHeader');
    input.value = obj.categoria;
    id.value = obj.id;
    form.action = url;
    text.textContent = 'Editar Categoria';
    modal.click();
    }

    /*Agregar nueva categoria*/
    function add(url) {
    var modal = document.getElementById('eventoModal');
    var input = document.getElementById('inputModal');
    var id = document.getElementById('id');
    var form = document.getElementById('formCat');
    var text = document.getElementById('txtHeader');
    input.value = '';
    id.value = '';
    form.action = url;
    text.textContent = 'Añadir nueva categoria';
    modal.click();
    }

    /*Agregar registros*/
    function save(){
    var form = $('#formCat');
    var url = form.attr('action');
    var data = form.serialize();
    $.post(url, data, function(msj){
    alert(msj);
    /*cerrar modal una vez efectuado el cambio*/
    $('#cerrarModal').click();
    location.reload(true);
    });
    }
   
    /*Eliminar*/
    function pasarId(id) {
    var idField = document.getElementById('idDelete');
    idField.value = id;
    var modal = document.getElementById('delete');
    modal.click();
    }
    /*Eliminar registro refrescar la pagina*/
    function deleteCat() {
    /*boton para cerrar modal despues del evento*/
    var closeWindow = document.getElementById('closeWindow');
    var id = document.getElementById('idDelete');
    var form = $("#formDelete");
    var url = form.attr('action');
    var data = form.serialize();
    $.post(url, data, function (msj) {
    alert(msj);
    if(msj=='Se ha eliminado el registro'){
    location.reload(true);
    }
    });
    closeWindow.click();
    }


</script>
<!-- fin script-->
@endsection

