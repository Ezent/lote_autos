@extends('plantilla.welcome') 
@section('navegacion')
<nav class="navbar navbar-default navbar-fixed-top" style="background: #FAFAFA">
    <div class="container-fluid">
        <div class="col-xs-1"></div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/"><img  src="{{asset('images/Logo.png')}}" height="70px" > </a>
            <a class="txtLogo" href="/" style="color: #000;"><b>ESPINOSA TRUKS</b></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="col-xs-1 "></div>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ver <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('panel')}}">Vehiculos</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{route('panel_categoria')}}">Categoria</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{route('slider')}}">Publicidad Inicio</a></li>
                        <!--                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>-->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Nuevo <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('panel_vehiculos')}}">Vehiculos</a></li>
                        <!--<li role="separator" class="divider"></li>-->
                        <!--<li><a href="#">Categorias</a></li>-->
                    </ul>
                </li>
            </ul>
            <div class="col-xs-1"></div>
            <div class="navbar navbar-nav ">
                <form id="form_buscar" action="{{route('buscar')}}" method="POST" onsubmit=" buscar_ajax(); return  false;">
                    @csrf
                    <div>
                        <div class="container-2">
                            <input onkeyup="buscar_ajax()" name="buscar" type="text" id="buscar" placeholder="..." />
                            <button style="border: none; background: none;" class="icon1" type="submit"><span ><i class="fa fa-search"></i></span></button>
                        </div>
                    </div>
                </form>
                <li id="a" class="dropdown" style="margin-top: 54px; display: none; position: absolute;">
                    <a id="activaBusqueda" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                    <ul class="dropdown-menu" style="width: 360px;">
                        <hr>
                        <div id="unaList" style="overflow: auto; max-height: 200px;"></div>
                    </ul>
                </li>
            </div>
            <div class="col-xs-2"></div>
            <div class="navbar navbar-nav">
                <ul class="nav navbar-nav">
                    <li class="dropdown ">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="fa fa-user"></span> {{auth()->user()->usuario}} <span class="caret"></span></a>
                        <ul class="dropdown-menu" style="width: 150px;">
                            <li role="separator" class="divider"></li>

                            <li>
                                <a href="{{route('panel_logout')}}">
                                    <span class="fa fa-user-circle"> </span>
                                    Salir
                                    </a>
                            </li>

                            <li role="separator" class="divider"></li>
                            <li><a href="{{url('/panel/categorias/user').'/'.auth()->user()->id}}">Configurar</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</nav>
<div id="espace"></div>
@endsection
 
@section('carrusel')
@endsection



<body style="background: #EEEDED">



    
@section('contenido')
@endsection




</body>




@section('pie')
@endsection
 
@section('js')
<!--<script src="{{asset('Tablas/vendor/jquery/jquery.min.js')}}"></script>-->

<!--<script src="{{asset('Tablas/vendor/bootstrap/js/bootstrap.min.js')}}"></script>-->

<script src="{{asset('Tablas/vendor/metisMenu/metisMenu.min.js')}}"></script>

<script src="{{asset('Tablas/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('Tablas/vendor/datatables-plugins/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('Tablas/vendor/datatables-responsive/dataTables.responsive.js')}}"></script>

<script src="{{asset('Tablas/dist/js/sb-admin-2.js')}}"></script>

<script>
    $(document).ready(function () {
                                    $('#dataTables-example').DataTable({
                                        responsive: true
                                    });
                                });

</script>
@endsection