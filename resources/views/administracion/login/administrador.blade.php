<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Iniciar Sesion</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->	
        <!--<link rel="icon" type="image/png" href=""/>-->
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('Login/vendor/bootstrap/css/bootstrap.min.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('Login/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('Login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('Login/vendor/animate/animate.css')}}">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="{{asset('Login/vendor/css-hamburgers/hamburgers.min.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('Login/vendor/animsition/css/animsition.min.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('Login/vendor/select2/select2.min.css')}}">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="{{asset('Login/vendor/daterangepicker/daterangepicker.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('Login/css/util.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('Login/css/main.css')}}">
        <!--===============================================================================================-->
    </head>
    <body>

        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
                    <form class="login100-form validate-form flex-sb flex-w" action="{{route('panel_login')}}" method="post">
                        @csrf
                        @if(!$errors->isEmpty())
                        <span id="msj" class="alert alert-danger">{{$errors->first('error')}}</span>
                        @endif
                        <span class="login100-form-title p-b-32">
                            INICIAR SESION
                        </span>
                        <span class="txt1 p-b-11">
                            Correo:
                        </span>
                        <div class="wrap-input100 validate-input m-b-36" data-validate = "El correo es requerido">
                            <input class="input100" type="email" name="correo"  value="{{old('correo')}}">
                            <span class="focus-input100"></span>
                        </div>

                        <span class="txt1 p-b-11">
                            Contraseña:
                        </span>
                        <div class="wrap-input100 validate-input m-b-12" data-validate = "El password es requerido">
                            <span class="btn-show-pass">
                                <i class="fa fa-eye"></i>
                            </span>
                            <input class="input100" type="password" name="pass" >
                            <span class="focus-input100"></span>
                        </div>
                        <div class="container-login100-form-btn">
                            <button type="submit" class="login100-form-btn">
                                Login
                            </button>
                        </div>
                        <div class="flex-sb-m w-full p-b-48">
                            <div class="contact100-form-checkbox">
<!--							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                                    <label class="label-checkbox100" for="ckb1">
                                            Remember me
                                    </label>-->
                            </div>

                            <div>
                                <a href="/" class="txt3">
                                    SALIR
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div id="dropDownSelect1"></div>

        <!--===============================================================================================-->
        <script src="{{asset('Login/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('Login/vendor/animsition/js/animsition.min.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('Login/vendor/bootstrap/js/popper.js')}}"></script>
        <script src="{{asset('Login/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('Login/vendor/select2/select2.min.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('Login/vendor/daterangepicker/moment.min.js')}}"></script>
        <script src="{{asset('Login/vendor/daterangepicker/daterangepicker.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('Login/vendor/countdowntime/countdowntime.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('Login/js/main.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $("#msj").fadeOut(1500);
    },4000);
});
</script>
    </body>
</html>