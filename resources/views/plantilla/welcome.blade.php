<!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Espinosa Truks.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Autos Espinosa Truks, te prestamos mas por tu vehiculos sin tener que dejarlo, con los intereses mas bajos del mercado"
    />
    <meta name="keywords" content="
              Autos, Autos Merida, Espinosa Truks, Autos Espinosa Truks, Empeño de Vehiculos, Auto-Empeño Merida,Auto-Empeño, Venta de Autos, Financiamiento de Autos,
              Consignaciones de Autos, Renta de Autos, Compra de Autos, Venta de Autos Usados, Autos Usados, Autos Usados Merida, Compra de Autos Usados Merida,
              Autos Baratos, Autos Baratos Merida, Empeños Autos, Empeño de Autos Merida, Autos Truks Merida, Espinosa Truks Merida, Espinosa Merida Merida Autos 
              " />
    <meta name="author" content="Autos Espinosa Truks" />

    <!-- Facebook and Twitter integration -->
    <!--        <meta property="og:title" content=""/>
                <meta property="og:image" content=""/>
                <meta property="og:url" content=""/>
                <meta property="og:site_name" content=""/>
                <meta property="og:description" content=""/>
                <meta name="twitter:title" content="" />
                <meta name="twitter:image" content="" />
                <meta name="twitter:url" content="" />
                <meta name="twitter:card" content="" />-->
    <!--Logo -->
    <link rel="icon" type="imge/x-icon" href="{{asset('images/Logo.png')}}">
    <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i" rel="stylesheet"> -->

    <!-- Animate.css -->
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{asset('css/icomoon.css')}}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">

    <!-- Flexslider  -->
    <link rel="stylesheet" href="{{asset('css/flexslider.css')}}">
    <!-- jQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- Flexslider -->
    <script src="{{asset('js/jquery.flexslider-min.js')}}"></script>

    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">

    <!-- Theme style  -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!--Estilos perosnalizados -->
    <link rel="stylesheet" href="{{asset('css/estilo.css')}}">
    <!--Efectos para el boton de busqueda -->
    <link rel="stylesheet" href="{{asset('css/botonBuscar.css')}}">
    <link href="http://designers.hubspot.com/hs-fs/hub/327485/file-2054199286-css/font-awesome.css" rel="stylesheet">

    <!--Script tag Google-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130747717-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
function gtag() {
    dataLayer.push(arguments);
}
gtag('js', new Date());

gtag('config', 'UA-130747717-1');
    </script>

    <!--end Script -->

    <!-- Modernizr JS -->
    <script src="{{asset('js/modernizr-2.6.2.min.js')}}"></script>
    <script type="text/javascript">
        $(window).load(function () {
    $('.flexslider').flexslider({
        animation: "slide"
    });
    $('#alternar').click(function () {
        $('#prueba').toggle();
    });
});
    </script>
    <style>
        .flex-caption {
            width: 96%;
            padding: 2%;
            left: 0;
            bottom: 0;
            background: rgba(0, 0, 0, .5);
            color: #fff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, .3);
            font-size: 14px;
            line-height: 18px;
        }
    </style>

    
@section('ext') @show

</head>

<body onclick="clic_cuerpo()">
    <div class="fh5co-loader"></div>
    <!--<div id="page">-->
    <div id="uno">

        
@section('navegacion')
        <header>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="col-xs-1"></div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                            aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        <a href="/"><img  src="{{asset('images/Logo.png')}}" height="70px" > </a>
                        <!--<a class="navbar-brand" href="/"  style="color: #000;"><b>ESPINOZA TRUKS</b></a>-->
                        <a class="txtLogo" href="/" style="color: #000;"><b>ESPINOSA TRUCKS</b></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <div class="col-xs-1 "></div>
                        <ul class="nav navbar-nav">
                            <li class="{{(Request::is('productos'))? 'active':''}}"><a href="/productos">Productos <span class="sr-only">(current)</span></a></li>
                            <li class="{{(Request::is('about'))? 'active':''}}"><a href="/about">¿Quiénes somos?</a></li>
                            <li class="{{(Request::is('servicios'))? 'active':''}}"><a href="/servicios">Servicios</a></li>
                            <li class="{{(Request::is('contacto'))? 'active':''}}"><a href="/contacto">Contacto</a></li>
                        </ul>
                        <div class="col-xs-1"></div>
                        <div class="navbar navbar-nav ">
                            <form id="form_buscar" action="{{route('buscar')}}" method="POST" onsubmit=" buscar_ajax();
                                        return  false;">
                                @csrf
                                <div>
                                    <div class="container-2">
                                        <input onkeyup="buscar_ajax()" name="buscar" type="text" id="buscar" placeholder="..." />
                                        <button style="border: none; background: none;" class="icon1" type="submit"><span ><i class="fa fa-search"></i></span></button>
                                    </div>
                                </div>
                            </form>
                            <li id="a" class="dropdown" style="margin-top: 54px; display: none; position: absolute;">
                                <a id="activaBusqueda" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                <ul class="dropdown-menu" style="width: 360px;">
                                    <hr>
                                    <div id="unaList" style="overflow: auto; max-height: 200px;"></div>
                                </ul>
                            </li>
                        </div>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
        </header>
        @show
        <div id="espace"></div>

        
@section('carrusel')
        <!--            <aside id="fh5co-hero" >
                            <div class="flexslider ">
                                <ul class="slides">
                                    <li class="p1"  style="background-image: url(images/img/autos/a1.1.jpeg)">
                                        <div class="overlay-gradient"></div>
                                        <div class="container">
                                            <div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text">
                                                <div class="slider-text-inner">
                                                    <div class="desc">
                                            <span class="price">$800</span>
                                                        <h2>Autos</h2>
                                                        <p>Contamos con una amplia gama de vehiculos de acorde a sus necesidades. Consulte nuestros productos y elija el mejor.</p>
                                                        <p><a href="/productos#AUTOS1" class="btn btn-primary btn-outline btn-lg">ver...</a></p>
                                                    </div>                                                                                                        
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li style="background-image: url({{asset('images/img/autos/p1.jpg')}});">
                                        <div class="container">
                                            <div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text">
                                                <div class="slider-text-inner">
                                                    <div class="desc">
                                                        <span class="price">$530</span>
                                                        <h2>Pick-ups</h2>
                                                        <p></p>
                                                        <p><a href="/productos#PICK-UPS1" class="btn btn-primary btn-outline btn-lg">ver...</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li style="background-image: url({{asset('images/img/comerciales/c6.jpg')}});">
                                        <div class="container">
                                            <div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text">
                                                <div class="slider-text-inner">
                                                    <div class="desc">
                                                        <span class="price">$750</span>
                                                        <h2>Comerciales</h2>
                                                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p>
                                                        <p><a href="/productos#CROSSOVER1" class="btn btn-primary btn-outline btn-lg">ver...</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li style="background-image: url({{asset('images/img/autos/a4.jpg')}});">
                                        <div class="container">
                                            <div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text">
                                                <div class="slider-text-inner">
                                                    <div class="desc">
                                                        <span class="price">$540</span>
                                                        <h2>Crossover y suvs</h2>
                                                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p>
                                                        <p><a href="/productos#COMERCIALES1" class="btn btn-primary btn-outline btn-lg">ver...</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li style="background-image: url({{asset('images/img/motos/m6.jpg')}});">
                                        <div class="container">
                                            <div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text">
                                                <div class="slider-text-inner">
                                                    <div class="desc">
                                                        <span class="price">$540</span>
                                                        <h2> Motos </h2>
                                                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p>
                                                        <p><a href="/productos#MOTOS1" class="btn btn-primary btn-outline btn-lg">ver...</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </aside>-->
        @show 
@section('contenido')
        <div id="fh5co-services" class="fh5co-bg-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 text-center">
                        <div class="feature-center animate-box" data-animate-effect="fadeIn">
                            <span class="icon">
                                    <i class="icon-credit-card"></i>
                                </span>
                            <h3>Vehiculos</h3>
                            <p>En autos Espinosa Truks contamos con una gran variedad de vehiculos que ponemos a tu alcance,
                                ven y pregunta por ellos.</p>
                            <p><a href="/productos" class="btn btn-primary btn-outline">ir...</a></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 text-center">
                        <div class="feature-center animate-box" data-animate-effect="fadeIn">
                            <span class="icon">
                                    <i class="icon-wallet"></i>
                                </span>
                            <h3>Servicios</h3>
                            <p>Tu nos importas!, ¿tienes dudas?.., autos Espinosa Truks pone a tu dispocision sus servicios,
                                acercate a nosotros y aclara tus dudas. </p>
                            <p><a href="/servicios" class="btn btn-primary btn-outline">ir..</a></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 text-center">
                        <div class="feature-center animate-box" data-animate-effect="fadeIn">
                            <span class="icon">
                                    <i class="icon-paper-plane"></i>
                                </span>
                            <h3>Financiamientos</h3>
                            <p>Queremos apoyarte en tu economia noostros te ofrecemos los mejores planes de finciamientos, serciorate
                                tu mismo y contactanos.</p>
                            <p><a href="/servicios/#financiamiento" class="btn btn-primary btn-outline">ir...</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="fh5co-product">
            <div class="container">
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                        <span>Algo interesante</span>
                        <h2>Algunas de las marcas de nuestros productos.</h2>
                        <p>Porque el cliente nos interesa, manejamos las mejores marcas del mercado para una mayor confianza
                            y garantia de nuestro productos. </p>
                    </div>
                </div>
                <div class="row">
                    <!--                        <div class="col-md-4 text-center animate-box">
                                                    <div class="product">
                                                        <div class="product-grid" style="background-image:url(images/product-1.jpg);">
                                                            <div class="inner">
                                                                <p>
                                                                    <a href="single.html" class="icon"><i class="icon-shopping-cart"></i></a>
                                                                    <a href="single.html" class="icon"><i class="icon-eye"></i></a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="desc">
                                                            <h3><a href="single.html">Hauteville Concrete Rocking Chair</a></h3>
                                                            <span class="price">$350</span>
                                                        </div>
                                                    </div>
                                                </div>-->
                    <div class="col-md-12">
                        <img class="img-fluid img-responsive" src="{{asset('images/img/marcas/logo-auto.jpg')}}">
                        <!--<span class="sale">Sale</span>-->
                    </div>
                </div>
            </div>
        </div>
        <div id="fh5co-counter" class="fh5co-bg fh5co-counter" style="background-image:url(images/img/autos/f4.jpg);">
            <div class="container">
                <div class="row">
                    <div class="display-t">
                        <div class="display-tc">
                            <div class="col-md-12 col-sm-6 animate-box">
                                <div class="feature-center">
                                    <!--                                    <span class="icon">
                                            <i class="icon-eye"></i>
                                        </span>-->

                                    <!--<span class="counter js-counter" data-from="0" data-to="22070" data-speed="5000" data-refresh-interval="50">1</span>-->
                                    <span class="counter-label">
                                            <b>Autos Espinosa Truks</b> te ofrece su amplio catálago de vehiculos nuevos y seminuevos, también ponemos a tu disposición nuestros
                                            servicios de renta, consignación y financiamiento, cualquier dudad o aclaración no dude en ponerse en contacto con nosotros y nuestro personal
                                            calificado le asesora.
                                        </span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--            <div id="fh5co-started" style="background: #C5C2A4    ">
                            <div class="container">
                                <div class="row animate-box">
                                    <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                                        <h2>Noticias!!!</h2>
                                        <p>Deseas recibir nuestras últimas noticias, promociones, rebajas, planes de finaciamientos, etc... No dudes y subscribete completamente gratis.</p>
                                    </div>
                                </div>
                                <div class="row animate-box">
                                    <div class="col-md-8 col-md-offset-2">
                                        <form class="form-inline" method="POST" action="#" onsubmit="return false;">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="email" class="sr-only">Email</label>
                                                    <input type="email" class="form-control" id="email" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <button type="submit" class="btn btn-default btn-block">Subscribirse</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>-->
        @show 
@section('pie')
        <div style="height: 60px;"></div>
        <footer id="fh5co-footer" role="contentinfo">
            <div class="container">
                <div class="row ">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 fh5co-widget">
                        <img class="img-responsive logo-footer" src="{{asset('images/Logo.png')}}" />
                    </div>
                </div>

                <div class="row copyright">
                    <div class="col-md-12 text-center">
                        <p>
                            <ul class="fh5co-social-icons fh5co-footer-links">
                                <li>
                                    <p><a href="/servicios#financiamiento">||  Empeños  ||</a></p>
                                </li>
                                <li>
                                    <p><a href="/servicios#consignaciones">||  Consignaciones ||</a></p>
                                </li>
                                <li>
                                    <p><a href="/productos">|| Ventas ||</a></p>
                                </li>
                                <li>
                                    <p><a href="/servicios#rentas">|| Rentas ||</a></p>
                                </li>
                            </ul>
                            <p></p>
                            <small class="block">&copy; Autos Espinosa Trucks. All Rights Reserved.</small>
                            <small class="block"> <span  class="glyphicon glyphicon-phone">Tel: </span> 999 194 2844 </small>
                        </p>
                        <p>
                            <ul class="fh5co-social-icons">
                                <li><a href="mailto:autodelsureste@gmail.com"><i class="icon-email"></i></a></li>
                                <li><a href="https://www.facebook.com/autos.espinosa.trucks"><i class="icon-facebook"></i></a></li>
                            </ul>
                        </p>
                    </div>
                </div>

            </div>
        </footer>
        @show
    </div>
    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
    </div>
    <!-- jQuery Easing -->
    <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- Waypoints -->
    <script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
    <!-- Carousel -->
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <!-- countTo -->
    <script src="{{asset('js/jquery.countTo.js')}}"></script>
    <!-- Main -->
    <script src="{{asset('js/main.js')}}"></script>

    <!--javascript para la busqueda -->
    <script type="text/javascript">
        /*Vienvenido a la funcion de ajax con jquery*/
                                                var abrirBusqueda = "cerrado";
                                                function clic_cuerpo() {
                                                    abrirBusqueda = "cerrado";
                                                }
                                                function buscar_ajax() {
                                                    /*imprimir algo*/
                                                    var txt = document.getElementById('buscar');
                                                    var botonActiva = document.getElementById('activaBusqueda');
                                                    var a = document.getElementById('a');
                                                    a.style.display = "block";
//        var m=document.getElementById('busqueda');
                                                    var form = $('#form_buscar');
                                                    var url = form.attr('action');
                                                    var datos = form.serialize();
                                                    $.post(url, datos, function (respuesta) {
                                                        var lista = respuesta.map(function (bar) {
                                                            return '<li class="pointer resultado" ><a href="' + bar.url + '">' + bar.nombre + '</a> | ' + bar.descripcion + '</li><hr>';
                                                        });
                                                        /*quitar comas*/
                                                        var cadena = lista.toString();
                                                        var arr = cadena.split(',');
                                                        var concat = '';
                                                        arr.forEach(function (element) {
                                                            concat += element;
                                                        });
                                                        document.getElementById('unaList').innerHTML = concat;
                                                        if (abrirBusqueda === "cerrado") {
                                                            botonActiva.click();
                                                            abrirBusqueda = "abierto";
                                                        }
                                                        $('#buscar').focus();
                                                        if (respuesta.length == 0) {
                                                            var respEmpty = '<li class="pointer resultado" ><a href="#">No hay resultados</a> | Prueba con otra frase :( </li><hr>';
                                                            document.getElementById('unaList').innerHTML = respEmpty;
                                                        }
                                                    });
                                                }
    </script>

    
@section('js') @show
</body>

</html>