@extends('plantilla.welcome') 
@section('carrusel')
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    @if (!$post->isEmpty())
    <ol class="carousel-indicators">
        @for ($i = 0; $i
        < count($post); $i++) @if ($i==0) <li data-target="#myCarousel" data-slide-to="0" class="active">
            </li>
            @else
            <li data-target="#myCarousel" data-slide-to="{{$i}}"></li>
            @endif @endfor
    </ol>
    @endif @if (!$post->isEmpty())
    <div class="carousel-inner">
        @for ($i = 0; $i
        < count($post); $i++) @if ($i==0) <div class="item active">
            <img id="altura1" src="{{$post[$i]->imagen}}">
    </div>
    @else
    <div class="item">
        <img id="altura1" src="{{$post[$i]->imagen}}">
    </div>
    @endif @endfor
</div>
@endif

<!-- Left and right controls -->
<a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
<a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
</div>
@endsection