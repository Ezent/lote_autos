@extends('plantilla.welcome') 
@section('ext')
<link rel="stylesheet" type="text/css" href="{{asset('dist/css/slider-pro.min.css')}}" media="screen" />
<!--<link rel="stylesheet" type="text/css" href="css/examples.css" media="screen"/>-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="{{asset('dist/jquery-1.11.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dist/js/jquery.sliderPro.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function ($) {
    /*Obtener todos los id */
    var ids=document.getElementsByName('idItems');
    var arr = ["#autos", "#pickups", "#crossover", "#comerciales", "#motos"];
    for (var i = 0; i < ids.length; i++) {

        $(ids[i]).sliderPro({
            width: 960,
            height: 500,
            fade: true,
            arrows: true,
            buttons: false,
            fullScreen: true,
            shuffle: true,
            smallSize: 500,
            mediumSize: 1000,
            largeSize: 3000,
            thumbnailArrows: true,
            autoplay: false
        });
    }

});

</script>
@endsection
 
@section('carrusel')
@endsection
 
@section('contenido')
<!--<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(images/img/a3.jpg); ">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <div class="display-t">
                    <div class="display-tc animate-box" data-animate-effect="fadeIn">
                        <h1>Nuestros Productos</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>-->
<div id="fh5co-header" class="contenedor" style="width: 100%; ">
    <img class="sombra act" style="height: 320px" src="{{asset('images/img/p1.jpeg')}}" />
    <h1 class="textEncimaCentrado letras">Productos</h1>
</div>
@if(!$msj=='')
<div class="alert alert-danger">
    <h3>
        <p align='center'>{{$msj}}</p>
    </h3>
</div>
@endif @if(!$cat->isEmpty()) @foreach($cat as $itemCat)
<div class="container">
    <div class="row animate-box">
        <div class="col-md-8 col-md-offset-2 text-center fh5co-heading" style="margin-top: 50px;">
            <span>Gama</span>
            <h2>{{$itemCat->categoria}}</h2>
        </div>
    </div>
</div>
<div name="idItems" id="{{$itemCat->categoria}}" class="slider-pro">
    <a id="{{$itemCat->categoria}}1"></a>
    <div class="sp-slides">
        @foreach($producto as $item) @if($itemCat->categoria==$item->categoria)
        <div class="sp-slide">
            <img class="sp-image img-responsive" src="{{$item->img}}" data-src="{{$item->img}}" />

            <p class="sp-layer sp-white sp-padding" data-horizontal="50" data-vertical="50" data-show-transition="left" data-show-delay="400">
                {{$item->nombre}}
            </p>

            <p class="sp-layer sp-black sp-padding" data-horizontal="480" data-vertical="50" data-show-transition="left" data-show-delay="600">
                {{$item->precio}}
            </p>

            <div class="sp-layer sp-white sp-padding" style="background: none" data-horizontal="50" data-vertical="100" data-show-transition="left"
                data-show-delay="800">
                <a href="/vista_vehiculos/{{$item->idVehiculo}}" title="Ver Vehiculo...">
                    <h1><span class="view icon icon-eye icon-xlg"></span></h1>
                </a>
            </div>
        </div>
        @endif @endforeach
    </div>
    <div class="sp-thumbnails">
        @foreach($producto as $item) @if($itemCat->categoria==$item->categoria)
        <img class="sp-thumbnail" src="{{$item->img}}" /> @endif @endforeach
    </div>
</div>
@endforeach @endif
@endsection