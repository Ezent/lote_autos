@extends('plantilla.welcome') 
@section('carrusel')
<div id="fh5co-header" class="contenedor" style="width: 100%">
    <img class="act" style="height: 320px" src="{{asset('images/img/qs2.jpg')}}" />
    <h1 class="textEncimaCentrado letras">¿Quiénes somos?</h1>
</div>
@endsection
 
@section('contenido')
<!--<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(images/img/a2.jpg);">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <div class="display-t">
                    <div class="display-tc animate-box" data-animate-effect="fadeIn">
                        <h1>¿Quienes somos?</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>-->
<div id="fh5co-about">
    <div class="container">
        <div class="about-content">
            <div class="row animate-box">
                <div class="col-md-6">
                    <div class="desc">
                        <h3>Mission</h3>
                        <p>Somos una empresa comprometida con las politicas de seguridad y servicios para lograr la maxima satisfacion
                            de nuestro clientes</p>
                    </div>
                    <div class="desc">
                        <h3>Vission</h3>
                        <p>Ser una empresa reconocida por nuestro gran profesionalismo que ofresca en nuestros productos y servicios
                            la absoluta confianza y calidad</p>
                    </div>
                    <div class="desc">
                        <h3>Nuestros Valores</h3>
                        <p>Servicio, Honestidad, Respeto, Desarrollo y Profesionalismo</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img class="img-responsive" src="images/img/qs.jpg" alt="about">
                </div>
            </div>
        </div>
        <!--        <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                        <span>Productive Staff</span>
                        <h2>Meet Our Team</h2>
                        <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeIn">
                        <div class="fh5co-staff">
                            <img src="images/person1.jpg" alt="Free HTML5 Templates by gettemplates.co">
                            <h3>Jean Smith</h3>
                            <strong class="role">Chief Executive Officer</strong>
                            <p>Quos quia provident consequuntur culpa facere ratione maxime commodi voluptates id repellat velit eaque aspernatur expedita. Possimus itaque adipisci.</p>
                            <ul class="fh5co-social-icons">
                                <li><a href="#"><i class="icon-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-dribbble"></i></a></li>
                                <li><a href="#"><i class="icon-github"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeIn">
                        <div class="fh5co-staff">
                            <img src="images/person2.jpg" alt="Free HTML5 Templates by gettemplates.co">
                            <h3>Hush Raven</h3>
                            <strong class="role">Co-Owner</strong>
                            <p>Quos quia provident consequuntur culpa facere ratione maxime commodi voluptates id repellat velit eaque aspernatur expedita. Possimus itaque adipisci.</p>
                            <ul class="fh5co-social-icons">
                                <li><a href="#"><i class="icon-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-dribbble"></i></a></li>
                                <li><a href="#"><i class="icon-github"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 animate-box" data-animate-effect="fadeIn">
                        <div class="fh5co-staff">
                            <img src="images/person3.jpg" alt="Free HTML5 Templates by gettemplates.co">
                            <h3>Alex King</h3>
                            <strong class="role">Co-Owner</strong>
                            <p>Quos quia provident consequuntur culpa facere ratione maxime commodi voluptates id repellat velit eaque aspernatur expedita. Possimus itaque adipisci.</p>
                            <ul class="fh5co-social-icons">
                                <li><a href="#"><i class="icon-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-dribbble"></i></a></li>
                                <li><a href="#"><i class="icon-github"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>-->
    </div>
</div>
@endsection