@extends('plantilla.welcome') 
@section('carrusel')
@endsection
 
@section('contenido')
<div id="fh5co-header" class="contenedor" style="width: 100%">
    <img class="sombra act" style="height: 320px" src="{{asset('images/img/s1.jpg')}}" />
    <h1 class="textEncimaCentrado letras">Servicios</h1>
</div>

<!--Aqui van todos nuestros servicios -->
<div style="background: #EEEDE9;">
    <div class="espaciado"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 contenedor">
                <img class=" img-responsive sombra " src="{{asset('images/img/f1.jpg')}}" />
                <h2 class=" textEncimaCentrado letras2">Auto Empeño</h2>
                <a id="financiamiento"></a>
            </div>
            <div class="col-md-6">
                <h2 class="tema-text">¿Deseas empeñar tu automovil?</h2>
                <p align='center'>
                    Contamos con los mejores planes de financiamientos para tu automovil, sin tramites largos y tediosos, y sin menospreciar
                    tu vehiculo. Acude a nuestras oficinas y un asesor financiero te asesorara.
                </p>
                <p align="center">
                    También puedes solicitar un prestamo para adquirir ese vehiculo que necesitas, te damos los prestamos mas altos y con los
                    intereses mas bajos.
                </p>
            </div>
        </div>
        <div style="height: 50px;"></div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="tema-text text-center">¿Quieres vender tu auto?...</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <img class="img-responsive  sombra" style="height: 25vw; width: 100%;" src="{{asset('images/img/c1.jpg')}}" />
                <h3 class="textEncimaCentrado letras">Consignaciones</h3>
                <a id="consignaciones"></a>
            </div>
        </div>
        <div class="espaciado"></div>
        <div class="row">
            <div class="col-md-6">
                <span class="tema-text2"><b>¿No sabes como?</b></span>
                <p align='justify'>
                    En Autos<b> Espinosa Trucks</b> te ofrecemos asesoría para la venta de tu automovil. Acercate a nosostros
                    y no mal vendas tu automovil.
                </p>
            </div>
            <div class="col-md-6">
                <span class="tema-text2"><b>¿Te compramos tu auto?</b></span>
                <p align='justify'>
                    No regales tu auto, llamanos o acude a nuestras instalaciones, te cotizamos precios sin ningun compromiso y recuerda que
                    Autos
                    <b>Espinosa Trucks</b> tiene un mejor precio para tu auto.
                </p>
            </div>
        </div>
        <div class="espaciado"></div>
        <div class="row">
            <div class="col-md-6">
                <h3 class="tema-text text-center">¿No tienes como moverte en la ciudad?...</h3>
                <span class="tema-text2">Nosotros te <b>rentamos</b> un auto...</span>
                <p align='justify'>
                    <b>Autos Trucks</b> pone a su dispocisión sus autos para servicio de renta, que nada te limite, avanza
                    en tu camino, disfruta de la ciudad y sus paisajes, ve mas halla de la ciudad y explora cada uno de sus
                    rincones, transportate en uno de nuestros autos que ponemos a tu dispocision y aprovecha nuestras ofertas,
                    contactanos al telefono <span class="text-nowrap icon icon-phone"> <u>999 194 2844</u></span> y uno de
                    nuestros asesores te atendera amablemente.
                </p>
            </div>
            <div class="col-md-6 contenedor">
                <img class=" img-responsive sombra " src="{{asset('images/img/r2.jpg')}}" />
                <h2 class=" textEncimaCentrado letras">Rentas</h2>
                <a id="rentas"></a>
            </div>
        </div>
        <div style="height: 70px;"></div>
    </div>
</div>
@endsection