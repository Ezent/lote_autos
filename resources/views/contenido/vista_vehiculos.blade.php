@extends('plantilla.welcome') 
@section('ext')
<link rel="stylesheet" type="text/css" href="{{asset('dist/css/slider-pro.min.css')}}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{asset('dist/css/examples.css')}}" media="screen" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="{{asset('dist/jquery-1.11.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dist/js/jquery.sliderPro.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function ($) {
//    $('#example5').sliderPro({
//        width: 670,
//        height: 500,
//        orientation: 'vertical',
//        loop: false,
//        arrows: true,
//        buttons: false,
//        thumbnailsPosition: 'right',
//        thumbnailPointer: true,
//        thumbnailWidth: 290,
//        breakpoints: {
//            800: {
//                thumbnailsPosition: 'bottom',
//                thumbnailWidth: 270,
//                thumbnailHeight: 100
//            },
//            500: {
//                thumbnailsPosition: 'bottom',
//                thumbnailWidth: 120,
//                thumbnailHeight: 50
//            }
//        }
//    });

    $('#example5').sliderPro({
        width: 960,
        height: 500,
        fade: true,
        arrows: true,
        buttons: false,
        fullScreen: true,
        shuffle: true,
        smallSize: 500,
        mediumSize: 1000,
        largeSize: 3000,
        thumbnailArrows: true,
        autoplay: false
    });
});

</script>
@endsection
 
@section('carrusel')
@endsection
 
@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-md" style="position: relative;  text-align: center;">
            <p align="center"><img class="img-responsive img-fluid" src="/{{$vehiculo->img}}" /> </p>
            <div style=" position: absolute; top: 12px;right: 90px;">
                <h3 class=" sp-black "> {{($vehiculo->descripcion=='NO')?'':$vehiculo->descripcion}}</h3>
            </div>
            <div style=" position: absolute; top: 40px;right: 90px; background: #595757;">
                <h2 style="color: white;">{{$vehiculo->precio}}</h2>
            </div>
        </div>
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading" style="margin-top: 50px;">
                <span>Atreve a explorar nuestro auto</span>
                <h2>Atributos</h2>
            </div>
        </div>
    </div>
    <!--Aqui empieza el carrusel con sus interiores y exteriores -->
    <div id="example5" class="slider-pro">
        <div class="sp-slides">
            @foreach($interiores as $item)
            <div class="sp-slide">
                <img class="sp-image img-responsive" src="/{{$item->interiores}}" data-src="/{{$item->interiores}}" data-retina="/{{$item->interiores}}"
                /> @if($item->descripcion!='NO')
                <p class="sp-layer sp-black sp-padding" data-position="bottomLeft" data-show-transition="up" data-hide-transition="down">

                    {{$item->descripcion}}
                    <!--                    <span class="hide-small-screen">,
                    consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </span> 
                <span class="hide-medium-screen">
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </span>-->
                </p>
                @endif
            </div>
            @endforeach @foreach($exteriores as $item)
            <div class="sp-slide">
                <img class="sp-image" src="/{{$item->exteriores}}" data-src="/{{$item->exteriores}}" data-retina="{{$item->exteriores}}"
                /> @if($item->descripcion!='NO')
                <p class="sp-layer sp-black sp-padding" data-position="bottomLeft" data-show-transition="up" data-hide-transition="down">
                    {{$item->descripcion}}
                </p>
                @endif
            </div>
            @endforeach
        </div>
        <div class="sp-thumbnails">
            @foreach($interiores as $item)
            <div class="sp-thumbnail">
                <div class="sp-thumbnail-image-container">
                    <img class="sp-thumbnail-image" src="/{{$item->interiores}}" />
                </div>
                <div class="sp-thumbnail-text">
                    <div class="sp-thumbnail-title">Interiores</div>
                    <div class="sp-thumbnail-description">{{$item->descripcion}}</div>
                </div>
            </div>
            @endforeach @foreach($exteriores as $item)
            <div class="sp-thumbnail">
                <div class="sp-thumbnail-image-container">
                    <img class="sp-thumbnail-image" src="/{{$item->exteriores}}" />
                </div>
                <div class="sp-thumbnail-text">
                    <div class="sp-thumbnail-title">Exteriores</div>
                    <div class="sp-thumbnail-description">{{$item->descripcion}}</div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
@endsection