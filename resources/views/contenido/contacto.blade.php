@extends('plantilla.welcome') 
@section('carrusel')
@endsection
 
@section('contenido')
<div id="fh5co-header" class="contenedor" style="width: 100%">
    <img class="sombra act" style="height: 320px" src="{{asset('images/img/c.jpg')}}" />
    <h1 class="textEncimaCentrado letras">Contactanos</h1>
</div>
<div id="fh5co-contact">
    <div class="container">
        <div class="row">
            <div class="col-md">
                <h2>Nuestra Ubicación</h2>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1862.1644757823494!2d-89.6435362!3d21.0195199!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjHCsDAxJzExLjIiTiA4OcKwMzgnMzQuOCJX!5e0!3m2!1ses!2smx!4v1544898444167"
                    width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="row" style="margin-top: 60px;">
            <div class="col-md-5 col-md-push-1 animate-box">

                <div class="fh5co-contact-info">
                    <h3>Información de contacto</h3>
                    <ul>
                        <li class="address">Calle 30 #281 por 15, Chuburna Hidalgo, <br> cp. 97205, Merida, Mex.</li>
                        <li class="phone"> Linea 1: 999 285 5442</li>
                        <li class="phone"> Linea 2: 999 285 5520</li>
                        <li class="icon-facebook"><a href="https://www.facebook.com/autos.espinosa.trucks">Espinosa Trucks</i></a></li>
                        <li class="email"><a href="mailto:autodelsureste@gmail.com">autodelsureste@gmail.com</a></li>
                        <li class="url">Gerente. Francisco Javier Espinosa</li>
                        <li class="phone">999 194 2844</li>
                    </ul>
                </div>

            </div>
            <div class="col-md-6 animate-box">
                <h3>Dudas !!!, Escribenos...</h3>
                <a id="email"></a>
                <form id="sendEmail" action="{{route('sendEmail')}}" onsubmit="send(); return false;" method="post">
                    @csrf
                    <div class="row form-group">
                        <div class="col-md-6">
                            <!-- <label for="fname">First Name</label> -->
                            <input required type="text" id="fname" name="nombre" class="form-control" placeholder="Nombre">
                        </div>
                        <div class="col-md-6">
                            <!-- <label for="lname">Last Name</label> -->
                            <input required type="text" id="lname" name="apellidos" class="form-control" placeholder="Apellidos">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <!-- <label for="email">Email</label> -->
                            <input required type="email" id="email" name="correo" class="form-control" placeholder="Dirección de correo">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <!-- <label for="subject">Subject</label> -->
                            <input required type="text" id="subject" name="asunto" class="form-control" placeholder="Asunto de tu mensaje">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <!-- <label for="message">Message</label> -->
                            <textarea required name="msj" id="message" name="msj" cols="30" rows="10" class="form-control" placeholder="Escribe tu mensaje detalladamente"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Enviar Mensaje" class="btn btn-primary">
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    function send() {
        var form = $('#sendEmail');
        var url = form.attr('action');
        var data = form.serialize();
        $.post(url, data, function (msj) {
            alert(msj);
            /*Limpir los campos*/
            var nombre = document.getElementsByName('nombre');
            var apellidos = document.getElementsByName('apellidos');
            var correo = document.getElementsByName('correo');
            var asunto = document.getElementsByName('asunto');
            var msj = document.getElementsByName('msj');
            /*Borrado*/
            nombre[0].value = "";
            apellidos[0].value = "";
            correo[0].value = "";
            asunto[0].value = "";
            msj[0].value = "";
        }).fail(function(error,status, errorThrown){
            alert(status+': '+errorThrown);
        });
    }

</script>
@endsection